@extends('layouts.UserLayout')
@section('title','Karma Payment')
@section('content')
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
			<div class="col-first">
				<h1>Checkout</h1>
				<nav class="d-flex align-items-center">
					<a href="index.html">Shop<span class="lnr lnr-arrow-right"></span></a>
					<a href="single-product.html">Checkout</a>
				</nav>
			</div>
		</div>
	</div>
</section>
<div class="container w-50">
	<form accept-charset="UTF-8" class="require-validation"
	action="{{ route('stripe.post') }}"
	data-cc-on-file="false"
	data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
	id="payment-form" method="post">
	@csrf
		<div class='form-row'>
			<div class='col-xs-12 form-group required'>
				<label class='control-label'>Name on Card</label> <input
				class='form-control' size='4' type='text'>
			</div>
		</div>
		<div class='form-row'>
			<div class='col-xs-12 form-group card required'>
				<label class='control-label'>Card Number</label> <input
				autocomplete='off' class='form-control card-number' size='20'
				type='text'>
			</div>
		</div>
		<div class='form-row'>
			<div class='col-xs-4 form-group cvc required'>
				<label class='control-label'>CVC</label> <input autocomplete='off'
				class='form-control card-cvc' placeholder='ex. 311' size='4'
				type='text'>
			</div>
			<div class='col-xs-4 form-group expiration required'>
				<label class='control-label'>Expiration</label> <input
				class='form-control card-expiry-month' placeholder='MM' size='2'
				type='text'>
			</div>
			<div class='col-xs-4 form-group expiration required'>
				<label class='control-label'> </label> <input
				class='form-control card-expiry-year' placeholder='YYYY' size='4'
				type='text'>
			</div>
		</div>
		<div class='form-row'>
			<div class='col-md-12'>
				<div class='form-control total btn btn-info'>
					Total: <span class='amount'>${{ $total + 50}}.00</span>
				</div>
			</div>
		</div>
		<div class='form-row'>
			<div class='col-md-12 form-group'>
				<button class='form-control btn btn-primary submit-button'
				type='submit' style="margin-top: 10px;">Pay »</button>
			</div>
		</div>
		<div class='form-row'>
			<div class='col-md-12 error form-group hide'>
				<div class='alert-danger alert'>Please correct the errors and try
				again.</div>
			</div>
		</div>
</form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	$(function() {
		var $form         = $(".require-validation");
		$('form.require-validation').bind('submit', function(e) {
			var $form         = $(".require-validation"),
			inputSelector = ['input[type=email]', 'input[type=password]',
			'input[type=text]', 'input[type=file]',
			'textarea'].join(', '),
			$inputs       = $form.find('.required').find(inputSelector),
			$errorMessage = $form.find('div.error'),
			valid         = true;
			$errorMessage.addClass('hide');

			$('.has-error').removeClass('has-error');
			$inputs.each(function(i, el) {
				var $input = $(el);
				if ($input.val() === '') {
					$input.parent().addClass('has-error');
					$errorMessage.removeClass('hide');
					e.preventDefault();
				}
			});

			if (!$form.data('cc-on-file')) {
				e.preventDefault();
				Stripe.setPublishableKey($form.data('stripe-publishable-key'));
				Stripe.createToken({
					number: $('.card-number').val(),
					cvc: $('.card-cvc').val(),
					exp_month: $('.card-expiry-month').val(),
					exp_year: $('.card-expiry-year').val()
				}, stripeResponseHandler);
			}

		});

		function stripeResponseHandler(status, response) {
			if (response.error) {
				$('.error')
				.removeClass('hide')
				.find('.alert')
				.text(response.error.message);
			} else {
           // token contains id, last4, and card type
           var token = response['id'];
           // insert the token into the form so it gets submitted to the server
           $form.find('input[type=text]').empty();
           $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
           $form.get(0).submit();
       }
   }

});
</script>
@endsection