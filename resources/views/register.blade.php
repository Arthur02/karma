@extends('layouts.UserLayout')
	@section('title','Karma Register')
	@section('content')
	<!-- Start Header Area -->
	
	<!-- End Header Area -->

	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Register/Login</h1>
					<nav class="d-flex align-items-center">
						<a href="home">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="login">Register/Login</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Login Box Area =================-->
	<section class="login_box_area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_form_inner">
						<h3>Register new account</h3>
						<form class="row login_form" action="{{ url('registration') }}" method="post" id="contactForm" novalidate="novalidate">
							@csrf
							<div class="col-md-12 form-group">
								@if($errors->has('name'))
									<label for="firstName" class="text-danger">{{ $errors->first('name') }}</label>
								@else
									<label for="firstName" class="col-sm-3 control-label">Name</label>
								@endif
							<input type="text" name="name" id="name" placeholder="First Name" value="{{ old('name') }}" class="form-control" autofocus>
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('surname'))
									<label for="lastName" class="text-danger">{{ $errors->first('surname') }}</label>
								@else	
									<label for="lastName" class="col-sm-3 control-label">Surname</label>
								@endif
								<input type="text" name="surname" id="surname" placeholder="Last Name" value="{{ old('surname') }}" class="form-control" autofocus>
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('age'))
									<label for="lastName" class="text-danger">{{ $errors->first('age') }}</label>
								@else
									<label for="age" class="col-sm-3 control-label">Age</label>
								@endif
								<input type="text" name="age" id="age" class="form-control" placeholder="Age" value="{{ old('age') }}">
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('email'))
									<label for="lastName" class="text-danger">{{ $errors->first('email') }}</label>
								@else
									<label for="email" class="col-sm-3 control-label">Email </label>
								@endif
									<input type="email" name="email" id="email" placeholder="Email" class="form-control" value="{{ old('email') }}">
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('password'))
									<label for="lastName" class="text-danger">{{ $errors->first('password') }}</label>
								@else
									<label for="password" class="col-sm-3 control-label">Password</label>
								@endif
							<input type="password" name="password" id="password" placeholder="Password" class="form-control">
							</div>
							<div class="col-md-12 form-group">
						@if($errors->has('passwordc'))
							<label for="lastName" class="text-danger">{{ $errors->first('passwordc') }}</label>
						@else
							<label for="password" class="col-sm-3 control-label">Confirm</label>
						@endif
							<input type="password" name="passwordc" id="passwordc" placeholder="Conform Password" class="form-control">
							</div>
							<div class="col-md-12 form-group">
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" value="submit" class="primary-btn">Register</button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_box_img">
						<img class="img-fluid" src="img/login.jpg" alt="">
						<div class="hover">
							<h4>Alredy have an account?</h4>
							<p>There are advances being made in science and technology everyday, and a good example of this is the</p>
							<a class="primary-btn" href="login">Log in</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Login Box Area =================-->
	@endsection('content')