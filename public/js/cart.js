jQuery(document).ready(function($) {
	let token = $('meta[name="csrf-token"]').attr('content')
	$('.removefromcart').click(function(event) {
		let id = $(this).attr('id')
		$(this).parents('.incpetqaanpaymanjnjeq').remove()
		$.ajax({
			type:"post",
			url:"deletefromcart",
			data:{"_token":token, 'id':id},
			success:function(r){
				Swal.fire(
					'Good job!',
					'Deleted!',
					'success'
					)
			}
		})
	});
	let n = 0
	let test = 0
	$('.getupman').click(function(event) {
		let a = $(this).parent().find('.qty').val()
		let b = $(this).parents('.incpetqaanpaymanjnjeq').find('.ginnn').html()
		let id = $(this).attr('id')
		$.ajax({
			type:'post',
			url:'comeonhypemeup',
			data:{"_token":token,'id':id,'a':a},
			success:r =>{
				if(r != 'shatvretvercir' && r != 'tivgri'){
					r = JSON.parse(r)
					console.log(r)
					$(this).parents('.incpetqaanpaymanjnjeq').find('.obshiginy').html('$'+r['a']+".00")
					$('.subsax').html('$' + r['b'] + '.00')
					// n += +b
					
				}
				if(r == 'shatvretvercir'){
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'There is no more!',
					})
				}
				else{
					test = 0
				}
				if(r == 'tivgri'){
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Please input number!',
					})
					$(this).val(1)
				}
			}
		})
		$(this).parent().find('.qty').val(1+ +a)
	});
	$('.getdownman').click(function(event) {
		let a = $(this).parent().find('.qty').val()
		let b = $(this).parents('.incpetqaanpaymanjnjeq').find('.ginnn').html()
		let c = $(this).parents('.incpetqaanpaymanjnjeq').find('.obshiginy').html()
		let id = $(this).attr('id')
		
		let q = 0
		$.ajax({
			type:'post',
			url:'comeonhypemedown',
			data:{"_token":token,'id':id,'a':a},
			success:r =>{
				console.log(r)
				if(r != 'qichvretvercir' && r != 'tivgri'){
					r = JSON.parse(r)
					$(this).parents('.incpetqaanpaymanjnjeq').find('.obshiginy').html('$'+r['a']+".00")
					$('.subsax').html('$' + r['b'] + '.00')
					// n += +b
					
				}
				if(r == 'qichvretvercir'){
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'There is too few!',
					})
					test = 1
					
				}
				else{
					test = 0
				}
				if(r == 'tivgri'){
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Please input number!',
					})
					$(this).val(1)
				}
			}
		})
		if(test != 1){
			
			$(this).parent().find('.qty').val(+a -1 )
		}
		else{
			$(this).parent().find('.qty').val(+a +1 )
		}
	});


	$('.incpetqaanpaymanjnjeq').ready(function(){
		$('.incpetqaanpaymanjnjeq').each(function(index, r) {		
			let a = $(this).find('.qty').val()
			let b = $(this).find('.ginnn').html()
			let c = a * b
			n+=c
			$(this).find('.obshiginy').html('$' + c + '.00')
			
		});
		$('.subsax').html('$' + n + '.00')
	})


});