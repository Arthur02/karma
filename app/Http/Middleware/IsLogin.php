<?php

namespace App\Http\Middleware;
use App\Users;
use Closure;
use Session;
use Redirect;

class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session::has("id")){
            $user = Users::where('id',Session::get('id'))->first();
            if(isset($user)){
                return $next($request);
            }
            else{
                Session::flush();
                return Redirect::to('/')
                ->with('message','You must be logined');
            }
        }
        else{
            Session::flush();
            return Redirect::to('/')
            ->with('message','You must be logined');
        }
    }
}
