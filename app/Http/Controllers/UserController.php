<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Hash;
use Session;
use Redirect;
use Validator;
use Mail;
use App\Products;
use App\ProductPhoto;
use App\Cart;
use App\whishlist;
use App\Notifications;

class UserController extends Controller
{

	// GET FUNCTIONS
	public function RegistrationPage(){
		return view('register');
	}
	public function LoginPage(){
		return view('login');
	}
	public function HomePage(){
		$user = Users::where('id',Session::get('id'))->first();
        $products = Products::where('user_id', Session::get('id'))->with('photos')->take(3)->latest('id')->get();
        $whish = whishlist::where('user_id',Session::get('id'))
        ->with(['products','productimages'])->get();
        $saximonq = cart::where('user_id',session::get('id'))->get();

		return view('home')
		->with('user',$user)
		->with('products',$products)
		->with('cart',$saximonq)
		->with('whish',$whish);
	}
	public function logOut(){
		Session::flush();
		return Redirect::to('login');
	}
	public function ForgotPage(){
		return view('forgotpassword');
	}
	public function ChangepassPage(){
		return view('changepassword');
	}
	public function contact(){
		$user = Users::where('id',Session::get('id'))->first();
		$admins = Users::where('type','admin')->get();
		return view('contact',compact('user','admins'));
	}

	// POST FUNCTIONS
	public function UserRegistration(Request $r){
		// dd($r->input());
	 $validatedData = $r->validate([
       	'name' => 'required|min:2|max:12|string',
       	'surname' => 'required|min:2|max:12|string',
		'age' => 'required|max:130|integer|min:18',
		'email' => 'required|email|unique:users,email',
		'password' => 'required|min:9|',
		'passwordc' => 'required|same:password'
    ]);
		if (count($validatedData) > 0) {
			$user = new Users;
			$user->name = $r->name;
			$user->surname = $r->surname;
			$user->age = $r->age;
			$user->email = $r->email;
			$user->password = Hash::make($r->password);
			$user->save();
			$verification_key = md5($user->id.$user->email);
			$data = [
				'name'=>$user->name,
				'surname'=>$user->surname,
				'id' => $user->id,
				'verification_key' => $verification_key,
			];
			Mail::send('mail',$data, function($message) use($user){
				$message->to($user->email,'Karma Shop')->subject('USER VERIFICATION EMAIL');
				$message->from('arthurvoskanyan02@gmail.com', 'Admin ձյա');
			});
			echo "mail sent";
			return Redirect::to('/login')->with('success');
		}

		// else{
		// 	return Redirect::to('/login')->with('success');
		// }
	}
	public function activation($id, $verification_key){
		// dd($id, $verification_key);
		$user = Users::where('id', $id)->first();
		if($user){
			if($verification_key == md5($id.$user->email)){
				$user->active = 1;
				$user->save();
				session::flash('saxlavaance','1');
				return Redirect::to('/login')->with('success');
			}
		}

	}
	public function UserLogin(Request $r){
		$validator = Validator::make($r->all(), [
			'email' => 'required',
			'password' => 'required',
		]);
		$user = Users::where("email",$r->email)->first();
		$validator->after(function ($validator) use($user,$r) {
			if (!$user) {
				$validator->errors()->add('email', 'Email address is not registrated!');
			}
			else if (!Hash::check($r->password,$user["password"])) {
				$validator->errors()->add('password', 'Password is incorrect!');
			}
			else if($user->active != 1){
				$validator->errors()->add('email', 'Confirm your email!');
			}
			else if($user->blocktime > time()){
				$validator->errors()->add('email', 'Your account is blocked. Try agian later');
			}
		});
		if ($validator->fails()) {
			Session::flash("login-error",true);
			return Redirect::to('/login')
			->withErrors($validator)
			->withInput();
		}
		else{
			Session::put([
				'id' => $user["id"],
			]);
			if($user->type == 'admin'){
				Session::put('admin', $user['id']);
				return Redirect::to("/admin");
			}
			else{
				Users::where('id',session::get('id'))->update([
					'logintime' => date("Y/m/d"),
				]);
				return Redirect::to("/home");
			}
		}
	}
	public function changemyimage(Request $r){
		$img = $r->file("img");
		$id = session()->get('id');
		$url = uniqid().request()->img->getClientOriginalName();
		$img->move("userphotos/",$url);
		$affected = Users::where('id', session()->get('id'))
		->update(['image' => $url]);
		return Redirect::to("/home");
	}
	public function imforgottest(Request $r){
		$validator = Validator::make($r->all(), [
			'mail' => 'required|email',
		]);
		$user = Users::where("email",$r->mail)->first();
		$validator->after(function ($validator) use($user,$r) {
			if (!$user) {
				$validator->errors()->add('mail', 'Email address is not registrated!');
			}
			else if($user->active != 1){
				$validator->errors()->add('mail', 'Confirm your email!');
			}
		});
		if ($validator->fails()) {
			Session::flash("login-error",true);
			return Redirect::to('/forgotpage')
			->withErrors($validator)
			->withInput();
		}
		else{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < 6; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			$a = [
				'randomString'=>$randomString,
			];
			Mail::send('mail',$a, function($message) use($r){
				$message->to($r->mail,'Karma Shop')->subject('USER PASSWORD CHANGE');
				$message->from('arthurvoskanyan02@gmail.com', 'Admin ձյա');
			});
			$user = Users::where('email',$r->mail)->update([
				'emailcode' => $randomString
			]);
			Session::flash("saaaaaxnormala");
			Session::put("iragracemaily",$r->mail);
			return Redirect::to("/changepass");
		}

	}
	public function changemypassword(Request $r){
		$user = Users::where('email',session::get('iragracemaily'))->first();
		$a = $user->emailcode;
		$validator = Validator::make($r->all(), [
			'code' => 'required',
			'password' => 'required|min:9|',
			'confirmpassword' => 'required|same:password'
		]);
		$validator->after(function ($validator) use($user,$r,$a) {
			if ($a != $r->code) {
				$validator->errors()->add('code', 'You Enter wrong code!');
			}
		});
		if($validator->fails()){
			Session::flash("login-error",true);
			return Redirect::to("/changepass")
			->withErrors($validator)
			->withInput();
		}
		else{
			Users::where('email',session::get('iragracemaily'))->update([
				'password' => Hash::make($r->password),
				'emailcode' => null,
			]);
			session::flush();
			return Redirect::to("/login");
		}
	}







	// Ajax FUNCTIONS
	public function changemyname(Request $r){
		$name = $r->name;
		$surname = $r->surname;
		Users::where('id',session::get('id'))
		->update([
			'name' => $name,
			'surname' => $surname,
		]);
	}


}







 //    public function welcome() {
	//     return view('welcome');
	// }
	//  public function example() {
	//     return view('example')
	//     ->with("name","Arthur")
	//     ->with("array",['item 1', 'item2', 'item3', 'item4'])
	//     ->with('users',[
	//     	['name'=>'Karen', 'surname'=>'Grigoryan','age'=>25],
	//     	['name'=>'Armen', 'surname'=>'Vardanyan','age'=>25],
	//     	['name'=>'Karine', 'surname'=>'Ghazaryan','age'=>45],
	//     	['name'=>'Armine', 'surname'=>'Simonyan','age'=>18],
	//     	['name'=>'Mher', 'surname'=>'Mheryan','age'=>31],
	//     ]);
	// }
	// public function showusers(){
	// 	$users = users::get();
	// 	return view('showusers')
	// 	->with('users',$users);
	// }
