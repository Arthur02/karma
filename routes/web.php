<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\IsLogin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'UserController@welcome'); 
// Route::get('/example','UserController@example');
// Route::get('/showusers','UserController@showusers');



Route::group(['middleware' => ['IsLogin']], function () {
    Route::get('/home', 'UserController@HomePage'); 
	Route::get('/myprod', 'ProductController@MyProductPage'); 
	Route::get('/cart', 'ProductController@CartPage');
	Route::get('/whishlist', 'ProductController@WhishPage');
	Route::get('/checkme', 'ProductController@CheckoutPage');
	Route::get('/order_buy','ProductController@buyme');
	Route::get('conform','ProductController@conf');
	Route::get('notifications','ProductController@notifications');
	Route::get('contact','UserController@contact');

});
// Admin
Route::group(['middleware' => ['AdminCheck']], function(){
	Route::get('/admin', 'AdminController@LogIn');
	Route::get('/admin/{any}', 'AdminController@LogIn')->where('any','.*');
});

Route::get('/user/activate/{id}/{verification_key}', 'UserController@activation'); 



//User Get requests
Route::get('/register', 'UserController@RegistrationPage'); 
Route::get('/login', 'UserController@LoginPage'); 
Route::get('/logOut', 'UserController@logOut'); 
Route::get('/forgotpage', 'UserController@ForgotPage'); 
Route::get('/changepass', 'UserController@ChangepassPage'); 

//User Post Requests
Route::post('/registration', 'UserController@UserRegistration'); 
Route::post('/LoginUser', 'UserController@UserLogin'); 
Route::post('/changeing', 'UserController@changemyimage'); 
Route::post('/imforgot', 'UserController@imforgottest'); 
Route::post('/changemypassword', 'UserController@changemypassword'); 


//User ajax Requests
Route::post('/imchangingname', 'UserController@changemyname');

// Product Get requsets
Route::get('/', 'ProductController@AllProductPage');
Route::get('/addProduct', 'ProductController@addprod'); 
Route::get('/allProduct', 'ProductController@AllProductPage');
Route::get('/allProduct/{key}', 'ProductController@AllProductPage');
Route::get('shop/product/{id}', 'ProductController@SingleProductPage');
Route::get('/compare/{id1}/{id2}', 'ProductController@ComparePage');

// Product Post requsets

Route::post('/imadding', 'ProductController@imadding'); 
Route::post('/search_result','ProductController@search_result');
Route::post('/editaddimage','ProductController@editaddimage');

// Product ajax requests

Route::post('addtocart','ProductController@addtocart');
Route::post('/deleteproduct', 'ProductController@dltprod');
Route::post('showall', 'ProductController@showall');
Route::post('/changesingle', 'ProductController@changesingle');
Route::post('/editmyphoto', 'ProductController@editphoto');
Route::post('/deletefromcart', 'ProductController@deletefromcart');
Route::post('/comeonhypemeup', 'ProductController@comeonhypemeup');
Route::post('/comeonhypemedown', 'ProductController@comeonhypemedown');
Route::post('/addtowhish', 'ProductController@addtowhish');
Route::post('/deletemywhish', 'ProductController@deletemywhish');
Route::post('/gnacartmti', 'ProductController@gnacartmti');
Route::post('/gnapoxvir', 'ProductController@gnapoxvir');
Route::post('/esgnovem', 'ProductController@esgnovem');
Route::post('/getthisonesinfo', 'ProductController@getthisonesinfo');
Route::post('/ratingsystem', 'ProductController@ratingsystem');
Route::post('/gotocartmyfriend', 'ProductController@gotocartmyfriend');
Route::post('/imgoingtowhishlistsinglic', 'ProductController@imgoingtowhishlistsinglic');
Route::post('/comparing', 'ProductController@comparing');
Route::post('countnotifs', 'ProductController@countnotifs');
Route::post('getchaternameeeeee', 'ProductController@getchaternameeeeee');
Route::post('getmymessageswithhim', 'ProductController@getmymessageswithhim');
Route::post('sendingmessage', 'ProductController@sendingmessage');
Route::post('countmess', 'ProductController@countmess');



// Stripe get


Route::get('/stripe', 'StripeController@Stripe');

// Stripe post

Route::post('/stripe', 'StripeController@StripePost')->name('stripe.post');



// Admin GET




// Admin POST
