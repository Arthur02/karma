<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Hash;
use Session;
use Redirect;
use Validator;
use Response;
use App\Products;
use App\ProductPhoto;
use App\Cart;
use App\whishlist;
use App\Category;
use App\Order;
use App\Orderdetales;
use App\Ratings;
use App\Notifications;
use App\Messages;
use View;
// use Illuminate\Http\Response;
class ProductController extends Controller
{
  // Get Requests
    public function addprod(){
        $user = Users::where('id',Session::get('id'))->first();
        $category = Category::get();
        return view('addproduct')
        ->with('user',$user)
        ->with('category',$category);
    }
    public function MyProductPage(){
        $user = Users::where('id',Session::get('id'))->first();
        $products = Products::where('user_id', Session::get('id'))->latest('id')->paginate(12);
        $category = Category::get();
        return view('myproduct',compact('products','user'));

    }
    public function AllProductPage(Request $r){
        $user = Users::where('id',Session::get('id'))->first();
        $category = Category::get();
        if($r->key){
            Session::forget('key1');
            Session::forget('key2');
            Session::forget('key3');
            Session::forget('key5');
            Session::flash('key',true);
            Session::put('key1',$r->key);
            $products = Products::where('user_id','!=', Session::get('id'))->where('active',1)
            ->where('name','like', '%'.$r->key.'%')
            ->latest('id')
            ->paginate(12);
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
            return view('allproducts')
            ->with('products',$products)
            ->with('user',$user)
            ->with('category',$category);
        }
        else{
            Session::forget('key1');
            Session::forget('key2');
            Session::forget('key3');
            Session::forget('key5');
            $products = Products::where('user_id', '!=', Session::get('id'))->where('active',1)->latest('id')->paginate(12);
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
            return view('allproducts')
            ->with('products', $products)
            ->with('photos')
            ->with('user',$user)
            ->with('category',$category);
        }
    }
    public function SingleProductPage($id){
        $user = Users::where('id',Session::get('id'))->first();
        $product = Products::where('id',$id)->with(['category'])
        ->with('photos')->first();
        $category = Category::get();
        $review = Ratings::where('product_id',$id)->with('userner')->get();
        $astxer = Ratings::where('product_id',$id)->take(5)->get();
        $a = 0;
        $b = 0;
        $lastproducts = Orderdetales::latest('id')
        ->take(9)
        ->with('getprods')
        ->get();
        foreach ($review as $re) {
            $a+=$re->rate;
            $b++;
        }
        if($b != 0){            
            $product->rating = $a/$b;
        }
        else{
            $product->rating = 0;
        }
        $product->rewqan = $b;

        $cart = cart::where('product_id',$id)
        ->where('user_id',session::get('id'))->first();
        return view('singleproduct',compact('product','user','category','cart','review','astxer','lastproducts'));
    }
    public function CartPage(){
        $user = Users::where('id',Session::get('id'))->first();
        $products = cart::where('user_id',Session::get('id'))->with(['products','productimages'])->get();
        return view('cart',compact('user','products'));
        // with photos ով փոխել

    }
    public function WhishPage(){
        $user = Users::where('id',Session::get('id'))->first();
        $products = whishlist::where('user_id',Session::get('id'))
        ->with(['products','productimages'])->get();
        return view('whishlist',compact('user','products'));

    }

    public function CheckoutPage(){
        $user = Users::where('id',Session::get('id'))->first();
        $products = cart::where('user_id',Session::get('id'))->with(['products'])->get();
        $total = 0;
        foreach($products as $product) {
            $total += $product->count * $product->products->price;
        }
        return view('chekout',compact('user','products','total'));

    }


    public function buyme(){
        $products = cart::where('user_id',Session::get('id'))->with(['products'])->get();
        $total = 0;
        foreach($products as $product) {
            $total += $product->count * $product->products->price;
        }
        $carts = cart::where('user_id',session::get('id'))->get();
        $order = new Order;
        $order->user_id = Session::get("id");
        $order->total = $total;
        $order->save();

        foreach ($carts as $product) {
           $order_details = new Orderdetales;
           $order_details->order_id = $order->id;
           $order_details->product_id = $product->product_id;
           $order_details->count = $product->count;
           $order_details->save();
        }
        cart::where('user_id', session::get('id'))->delete();
        $order = Order::where('user_id',session::get('id'))->get();
        session::flash('normaluxarkvav','awesome');
        return Redirect::to('conform');
    }
    public function conf(){
        $user = Users::where('id',Session::get('id'))->first();
        $orders = Order::where('user_id',session::get('id'))
        ->with('detals')
        ->with('ordprod')
        ->latest('id')
        ->get();
        return view('/allorders',compact('orders','user'));
    }
    public function ComparePage($id1, $id2){
        $first = $id1;
        $sec = $id2;
        $prod1 = Products::where('id', $first)->first();
        $prod2 = Products::where('id', $sec)->first();
        return view('/compare',compact('prod1','prod2'));
    }
    public function notifications(){
        $user = Users::where('id',Session::get('id'))->first();
        $notifications = Notifications::where('user_id',session::get('id'))->latest()->get();
        Notifications::where('user_id',session::get('id'))->update([
            'seen' => 1
        ]);
        return view('notifications',compact('notifications','user'));
    }



  // Post Requests
    public function imadding(Request $r){
        $validate = $r->validate([
          'name' => 'required|min:2|', 
          'count' => 'required|min:1|integer', 
          'price' => 'required|numeric|min:1', 
          'photo.*' => 'required', 
          'photo' => 'min:1|required|max:6', 
          'type' => 'required', 
          'description' => 'required', 
      ]);


        $product = new Products;
        $product->name = $r->name;
        $product->price = $r->price;
        $product->count = $r->count;
        $product->description = $r->description;
        $product->user_id = Session::get('id');
        $product->category_id = $r->type;
        $product->save();

	// dd($r);   
        foreach ($r->file('photo') as $image) {
           $name = time().$image->getClientOriginalName();
           $image->move(public_path().'/img/product/',$name);
           $photo = new ProductPhoto;
           $photo->product_id = $product->id;
           $photo->photo = $name;
           $photo->save();
       }

       Session::put('success', "Product is successfully added");

    // dd(Session::get('success'));
       return Redirect::to('/addProduct');

   }
   public function search_result(Request $r){
        return Redirect::to('allProduct/'.$r->key);
    }









  // Product ajax Requests

public function dltprod(Request $r){
    Products::where('id', $r->id)->delete();
    ProductPhoto::where('product_id',$r->id)->delete();
}
public function showall(Request $r){
    $products = Products::where('user_id', '==', Session::get('id'))->get();
    print $products;
}
public function changesingle(Request $r){
    $rules =[
      'name' => 'required|min:2|', 
      'count' => 'required|min:1|integer', 
      'price' => 'required|numeric|min:1', 
      'type' => 'required', 
      'description' => 'required'
  ];
  $validator = Validator::make($r->all(), $rules);

  if ($validator->fails())
  {
      return Response::json(array(
          'success' => false,
          'errors' => $validator->getMessageBag()->toArray()

      ), 400);
  }
  else{
      $affected = Products::where('id', $r->id)
      ->update(
        [
          'name' => $r->name,
          'count' => $r->count,
          'price' => $r->price,
          'description' => $r->description,
          'category_id' => $r->type,
          'active' => 0,
      ]
  );
  }

}

public function editphoto(Request $r){
    ProductPhoto::where('id', $r->id)->delete();
}
public function editaddimage(Request $r){
    $a = ProductPhoto::where('product_id',$r->id)->get();
    $rules = [
          'photo.*' => 'required', 
          'photo' => 'min:1|required|max:6', 
    ];
    // dd($r->file('photo'));
    $validator = Validator::make($r->all(),$rules);
    if($r->file('photo') == null){
        return Redirect::to('shop/product/'.$r->id)
        ->withErrors($validator);
    }
    $validator->after(function ($validator) use($a,$r) {
        if(count($r->file('photo')) + count($a) > 6){
            $validator->getMessageBag()->add('photo', 'You can have Max 6 photos');
        }
    });
    if ($validator->fails()){
        return redirect()->back()->withErrors($validator);
    }
    if(!$validator->fails()){
        foreach ($r->file('photo') as $image) {
            $name = time().$image->getClientOriginalName();
            $image->move(public_path().'/img/product/',$name);
            $photo = new ProductPhoto;
            $photo->product_id = $r->id;
            $photo->photo = $name;
            $photo->save();
       }
       Products::where('id',$r->id)->update([
            'active' => 0,
       ]);
    }
    return Redirect::to('shop/product/'.$r->id);
}
public function addtocart(Request $r){
    if(!session::has('id')){
        print 'chunes';
    }
    else{     
        $cart = new cart;
        $id = $r->id;
        $a = Products::where('id',$id)->first();
        $ban = cart::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        // print_r($id);
        if($ban){
            if($ban->count + 1 <= $a->count ){
                cart::where('product_id',$id)
                ->where('user_id',session::get('id'))
                ->update([
                    'count' => $ban->count + 1
                ]);
            }
        }
        else{
            $cart->count = 1;
            $cart->product_id = $id;
            $cart->user_id = session::get('id');
            $cart->save();
        }
    }
}
public function deletefromcart(Request $r){
    $id = $r->id;
    cart::where('product_id',$id)
    ->where('user_id',session::get('id'))->delete();
}
public function comeonhypemeup(Request $r){
    $id = $r->id;
    $a = cart::where('id',$id)->first();
    $c = Products::where('id',$a->product_id)->first();
    $b = $r->a;
    if($b < $c->count && $b > 0 && $b != 'NaN'){
        cart::where('id',$id)->update([
            'count' => (int)++$b
        ]);
        $saximonq = cart::where('user_id',session::get('id'))->get();
        $n = 0;
        foreach($saximonq As $hatik){
            $o = Products::where('id',$hatik->product_id)->first();
            $g = (int)$hatik->count * (int)$o->price;
            $n+=$g;
        }
        print_r(json_encode(['a'=>$b * $c->price,'b'=>$n]));
    }
    else if($b >= $c->count){
        print 'shatvretvercir';
    }
    else if($b <= 0){
        print 'qichvretvercir';
    }
    else if($b == 'NaN'){
        print 'tivgri';
    }
}
public function comeonhypemedown(Request $r){
    $id = $r->id;
    $a = cart::where('id',$id)->first();
    $c = Products::where('id',$a->product_id)->first();
    $b = $r->a;
    if($b < $c->count  && $b > 1 && $b != 'NaN'){
        cart::where('id',$id)->update([
            'count' => (int)--$b
        ]);
        $saximonq = cart::where('user_id',session::get('id'))->get();
        $n = 0;
        foreach($saximonq As $hatik){
            $o = Products::where('id',$hatik->product_id)->first();
            $g = (int)$hatik->count * (int)$o->price;
            $n+=$g;
        }
        print_r(json_encode(['a'=>$b * $c->price,'b'=>$n]));
    }
    else if($b >= $c->count){
        print 'shatvretvercir';
    }
    else if($b <= 1){
        print 'qichvretvercir';
    }
    else if(!is_string($b)){
        print 'tivgri';
    }
}

public function addtowhish(Request $r){
    if(!session::has('id')){
        print 'chkas';
    }
    else{
        $whish = new whishlist;
        $id = $r->id;
        $a = Products::where('id',$id)->first();
        $ban = whishlist::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        // print_r($id);
        if($ban){
            print 'unessranic';
        }
        else{
            $whish->product_id = $id;
            $whish->user_id = session::get('id');
            $whish->save();
        }
    }
}
public function deletemywhish(Request $r){
    $id = $r->id;
    whishlist::where('id',$id)
    ->delete();
}
public function gnacartmti(Request $r){   
        $id = $r->id;
        whishlist::where('product_id',$id)
        ->where('user_id',session::get('id'))
        ->delete();
        $a = Products::where('id',$id)->first();
        $cart = new cart;
        $ban = cart::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        if($ban){
            if($ban->count + 1 <= $a->count ){
                cart::where('product_id',$id)
                ->where('user_id',session::get('id'))
                ->update([
                    'count' => $ban->count + 1
                ]);
            }
        }
        else{
            $cart->product_id = $id;
            $cart->user_id = session::get('id');
            $cart->count = 1;
            $cart->save();
        }
}
public function gnapoxvir(Request $r){
    session::put('key5',$r->id);
    if(!session::has('key1') && !session::has('key2')){    
        $id = $r->id;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('category_id',$id)->with('photos')->latest('id')->get();
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));
    }
    else if(session::has('key1') && !session::has('key2')){
        $id = $r->id;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('category_id',$id)
        ->where('name','like', '%'.session::get('key1').'%')
        ->with('photos')->latest()->get();
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));

    }
    else if(!session::has('key1') && session::has('key2')){
        $id = $r->id;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', session::get('key2'))
        ->where('price', '<=', session::get('key3'))
        ->where('category_id',$id)->with('photos')
        ->with('photos')
        ->latest('id')
        ->get();
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));
   }
   else if(session::has('key1') && session::has('key2')){
        $id = $r->id;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', session::get('key2'))
        ->where('price', '<=', session::get('key3'))
        ->where('category_id',$id)->with('photos')
        ->where('name','like', '%'.session::get('key1').'%')
        ->latest('id')
        ->with('photos')->get();
            foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));
   }
}
public function esgnovem(Request $r){
    session::put('key2',$r->a);
    session::put('key3',$r->b);
    if(!session::has('key1') && !session::has('key5')){
        $a = $r->a;
        $b = $r->b;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', $a)
        ->where('price', '<=', $b)
        ->with('photos')->latest('id')
        ->get();
        foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print json_encode($products);
    }
    else if(session::has('key1') && !session::has('key5')){
        $a = $r->a;
        $b = $r->b;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', $a)
        ->where('price', '<=', $b)
        ->where('name','like', '%'.session::get('key1').'%')->latest('id')
        ->with('photos')->get();
        foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));

    }
    else if(session::has('key5') && !session::has('key1')){
        $a = $r->a;
        $b = $r->b;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', $a)
        ->where('price', '<=', $b)
        ->where('category_id', session::get('key5'))->latest('id')
        ->with('photos')->get();
        foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));

    }
    else if(session::has('key5') && session::has('key1')){
        $a = $r->a;
        $b = $r->b;
        $products = Products::where('user_id', '!=', session::get('id'))
        ->where('active',1)
        ->where('price', '>=', $a)
        ->where('price', '<=', $b)
        ->where('category_id', session::get('key5'))
        ->where('name','like', '%'.session::get('key1').'%')->latest('id')
        ->with('photos')->get();
        foreach($products as $prod){
                $a = 0;
                $n = 0;
                $gnahat = Ratings::where('product_id',$prod->id)->get();
                foreach ($gnahat as $g) {
                    $a += $g->rate;
                    $n = $n + 1;
                }
                if($n != 0){
                    $p = $a/$n;
                }
                else{
                    $p = 0;
                }
                $prod->gnahatakan = (int)$p;
            }
        print(json_encode($products));

    }

}
    public function getthisonesinfo(Request $r){
        $id = $r->id;
        $order = Order::where('user_id',session::get('id'))
        ->where('id',$id)
        ->with('detals')
        ->with('ordprod')
        ->first();
        print(json_encode($order));
    }
    public function ratingsystem(Request $r){
        $id = $r->id;
        $message = $r->message;
        $gnahatakan = $r->gnahatakan;
        $gn = Ratings::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        if(!$gn){    
            $rate = new Ratings;
            $rate->product_id = $id;
            $rate->user_id = session::get('id');
            $rate->comment = $message;
            $rate->rate = (int)$gnahatakan;
            $rate->save();
        }
        else{
            Ratings::where('user_id',session::get('id'))
            ->where('product_id',$id)->update([
                'comment' => $message,
                'rate' => $gnahatakan,
            ]);
        }
    }
    public function gotocartmyfriend(Request $r){
        $id = $r->id;
        $qanak = $r->qanak;
        $max = $r->max;
        $kate = Cart::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        if($qanak <= $max){
            print_r ($kate);
            if(!$kate){           
                $cart = new Cart;
                $cart->user_id = session::get('id');
                $cart->product_id = $id;
                $cart->count = $qanak;
                $cart->save();
            }
            else{
                Cart::where('user_id',session::get('id'))
                ->where('product_id',$id)->update([
                    'count' => $qanak,
                ]);
            }
        }
        else{
            print 'no';
        }
    }
    public function imgoingtowhishlistsinglic(Request $r){
        $id = $r->id;
        $whish = whishlist::where('user_id',session::get('id'))
        ->where('product_id',$id)->first();
        if($whish){
            print 'karsranicaaaa';
        }
        else{
            $wh = new whishlist;
            $wh->user_id = Session::get('id');
            $wh->product_id = $id;
            $wh->save();
            print 'yesstacvecura';
        }
    }
    public function comparing(Request $r){
        $first = $r->first;
        $sec = $r->sec;
        $prod1 = Products::where('id', $first)->first();
        $prod2 = Products::where('id', $sec)->first();
        return Redirect::to('/compare/'.$first.'/'.$sec);
    }
    public function countnotifs(Request $r){
        $a = Notifications::where('user_id',session::get('id'))
        ->where('seen', 0)->get();
        print(count($a));
    }
    public function getchaternameeeeee(Request $r){
        $id = $r->id;
        $a = Users::where('id',$id)->first();
        print(json_encode($a));
    }
    public function getmymessageswithhim(Request $r){
        $id = $r->id;
        $m = Messages::where('user_id',$r->id)
        ->where('admin_id', session::get('id'))->orWhere('user_id',session::get('id'))->where('admin_id',$r->id)->get();
        Messages::where('user_id',session::get('id'))
        ->where('admin_id', $id)->update([
            'seen' => 1
        ]);
        print(json_encode($m));
    }
    public function sendingmessage(Request $r){
        $mes = new Messages;
        $mes->admin_id = Session::get('id');
        $mes->user_id = $r->id;
        $mes->message = $r->message;
        $mes->save();
    }
    public function countmess(Request $r){
        $a = Messages::where('user_id',session::get('id'))
        ->where('seen', 0)->get();
        print(count($a));
    }

}


          // 'photo.*' => 'required', 
        // $validator->errors()->add('photo', 'You can have Max 6 photos');
          // 'photo' => 'min:1|required|max:6', 
