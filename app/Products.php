<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function photos(){
   		return $this->hasMany("App\ProductPhoto", "product_id");
	}
	public function category(){
   		return $this->belongsTo("App\Category", "category_id");
	}
	public function user(){
		return $this->belongsTo('App\Users', "user_id");
	}
}
