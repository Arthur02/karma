@extends('layouts.UserLayout')
	@section('title','Karma Add product')
	@section('content')
	<!-- Start Header Area -->
	
	<!-- End Header Area -->

	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Add product</h1>
					<nav class="d-flex align-items-center">
						<a href="home">Shop<span class="lnr lnr-arrow-right"></span></a>
						<a href="login">Add product</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Login Box Area =================-->
	<section class="login_box_area section_gap">
		<div class="container w-100">
			<div class="row">
				<div class="w-100">
					<div class="login_form_inner">
						<h3>Add new product</h3>
					@if(Session()->has('success'))
						<label class="text-success mb-2">Product is successfully added</label><br>
						{{Session::forget('success')}}
					@endif
						<form class="row login_form" action="{{ url('imadding') }}" method="post" id="contactForm" novalidate="novalidate" enctype="multipart/form-data">
							@csrf
							<div class="col-md-12 form-group">
								@if($errors->has('name'))
									<label for="name" class="text-danger">{{ $errors->first('name') }}</label>
								@else
									<label for="name" class="col-sm-3 control-label">Name</label>
								@endif
							<input type="text" name="name" id="name" placeholder="Product Name" value="{{ old('name') }}" class="form-control" autofocus>
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('count'))
									<label for="lastName" class="text-danger">{{ $errors->first('count') }}</label>
								@else	
									<label for="lastName" class="col-sm-3 control-label">Count</label>
								@endif
								<input type="text" name="count" id="count" placeholder="Product count" value="{{ old('count') }}" class="form-control" autofocus>
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('price'))
									<label for="lastName" class="text-danger">{{ $errors->first('price') }}</label>
								@else
									<label for="price" class="col-sm-3 control-label">Price</label>
								@endif
								<input type="text" name="price" id="price" class="form-control" placeholder="Product price" value="{{ old('price') }}">
							</div>
							<div class="col-md-12 form-group">
								<div class="form-select" id="default-select">
								@if($errors->has('type'))
									<label for="lastName" class="text-danger">{{ $errors->first('type') }}</label>
								@else
									<label for="select" class="col-sm-3 control-label">Type</label>
								@endif
									<select name="type">
										<option disabled selected="">Product type</option>
										 @foreach($category as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                         @endforeach
									</select>
								</div>
							</div>
							<div class="col-md-12 form-group">
								@if($errors->has('photo.*'))
									<label for="photo.*" class="text-danger">{{ $errors->first('photo.*') }}</label>
								@elseif($errors->has('photo'))
									<label for="photo" class="text-danger">{{ $errors->first('photo') }}</label>
								@else
									<label for="photo" class="col-sm-3 control-label">Images</label>
								@endif
								<input type="file" name="photo[]" multiple accept="image/*">
							</div>
							<div class="col-md-12 form-group">
							<div class="mt-10">
								@if($errors->has('description'))
									<label for="lastName" class="text-danger">{{ $errors->first('description') }}</label>
								@else
									<label for="select" class="col-sm-3 control-label">Description</label>
								@endif
								<textarea class="single-textarea" name="description" placeholder="Product description" required="" value="{{ old('description') }}"></textarea>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" value="submit" class="primary-btn">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Login Box Area =================-->




	@endsection('content')