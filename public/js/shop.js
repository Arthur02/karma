jQuery(document).ready(function($) {
  let token = $('meta[name="csrf-token"]').attr('content')
	$(document).on('click', '.clickforbuy',function(event) {
		let id = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"/addtocart",
			data:{"_token":token,'id':id},
			success:function(r){
				if(r == 'chunes'){
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'You need to be loged in!',
					})
				}
				else{					
					console.log(r)
					Swal.fire(
					  'Good job!',
					  'Your product added to cart!',
					  'success'
					)
				}
			}
		})
	});
	$(document).on('click','.clickforwhish',function(event) {
		let id = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"/addtowhish",
			data:{"_token":token,'id':id},
			success:function(r){
					console.log(r)
				if(r == 'unessranic'){
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'You already have this in your whishlist!',
					})
				}
				else{
					
				console.log(r)
				Swal.fire(
				  'Good job!',
				  'Your product added to Whishlist!',
				  'success'
				)
				}
				if(r == 'chkas'){
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'You needto be loged in!',
					})
				}
			}
		})
	});
	$('.clickforchangefilteraaa').click(function(event) {
		let id = $(this).attr('data-id')
		console.log(id)
		$(this).find("input").prop('checked', true)
		$.ajax({
			type:'post',
			url:'/gnapoxvir',
			data:{'_token':token, 'id':id},
			success:function(r){
				r = JSON.parse(r)
				console.log(r)
				$('.sraaaaaaaaaaaaaaaaaaan').empty()
				r.forEach(function(e){
					console.log(e)
					if(e.count != 0){
						
					$('.sraaaaaaaaaaaaaaaaaaan').append(`
						<div class="col-lg-4 col-md-6">
							<div class="single-product">
								<div style="position:relative">
									<img class="img-fluid" src="/img/product/${e.photos[0].photo}" alt="">
									<div class='astxayingnahatmansistem' style="position: absolute;bottom: 23px; right: 10px;">
									</div>
									
								</div>
									<div class="product-details">
										<h6 class="asdasasdasdasdasdasd">${e.name}</h6>
										<div class="price">
											<h6>$${e.price}.00</h6>
											<h6 class="l-through">$${e.price + 50}.00</h6>
										</div>

										<div class="prd-bottom">

											<a class="social-info clickforbuy" style="cursor:pointer" id="${e.id}">
												<span class="ti-bag"></span>
												<p class="hover-text">add to bag</p>
											</a>
											<a class="social-info clickforwhish" style="cursor:pointer" id="${e.id}">
												<span class="lnr lnr-heart"></span>
												<p class="hover-text">Wishlist</p>
											</a>
											<a class="social-info clickforcompaere" style="cursor:pointer" id="${e.id}">
												<span class="lnr lnr-sync"></span>
												<p class="hover-text">compare</p>
											</a>
											<a href="/shop/product/${e.id}" class="social-info">
												<span class="lnr lnr-move"></span>
												<p class="hover-text">view more</p>
											</a>
										</div>
									</div>
								</div>
							</div>
						`)
						for(let i = 0; i<5; i++){
							let a = $('.single-product').last().find('.astxayingnahatmansistem')
							if(i < e.gnahatakan){
								$(`<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>`).appendTo(a);
							}
							else{
								$(`<i class="fa fa-star" aria-hidden="true" style="font-size: 15px"></i>`).appendTo(a)
							}
						}
					}
				})
			}
		})
	});
	$('label.dirincevent').click(function(event) {
   		event.preventDefault();
    	$(this).parent().find("input").prop('checked', true);
  });
$('.esfiltrumem').click(function(event) {
	let a = $(this).parent().find('#lower-value').html()
	a = a.split('.')
	a = a[0]
	let b = $(this).parent().find('#upper-value').html()
	b = b.split('.')
	b = b[0]
	console.log(+a)
	console.log(+b)
	$.ajax({
		type:'post',
		url:'/esgnovem',
		data:{"_token":token,'a':+a,'b':+b},
		success:function(r){
				r = JSON.parse(r)
				console.log(r)
				$('.sraaaaaaaaaaaaaaaaaaan').empty()
				r.forEach(function(e){
					console.log(e)
					if(e.count != 0){
						
					$('.sraaaaaaaaaaaaaaaaaaan').append(`
						<div class="col-lg-4 col-md-6">
							<div class="single-product">
								<div style="position:relative">
									<img class="img-fluid" src="/img/product/${e.photos[0].photo}" alt="">
									<div class='astxayingnahatmansistem' style="position: absolute;bottom: 23px; right: 10px;">
									</div>
									
								</div>
									<div class="product-details">
										<h6 class="asdasasdasdasdasdasd">${e.name}</h6>
										<div class="price">
											<h6>$${e.price}.00</h6>
											<h6 class="l-through">$${e.price + 40}.00</h6>
										</div>

										<div class="prd-bottom">

											<a class="social-info clickforbuy" style="cursor:pointer" id="${e.id}">
												<span class="ti-bag"></span>
												<p class="hover-text">add to bag</p>
											</a>
											<a class="social-info clickforwhish" style="cursor:pointer" id="${e.id}">
												<span class="lnr lnr-heart"></span>
												<p class="hover-text">Wishlist</p>
											</a>
											<a class="social-info clickforcompaere" style="cursor:pointer" id="${e.id}">
												<span class="lnr lnr-sync"></span>
												<p class="hover-text">compare</p>
											</a>
											<a href="/shop/product/${e.id}" class="social-info">
												<span class="lnr lnr-move"></span>
												<p class="hover-text">view more</p>
											</a>
										</div>
									</div>
								</div>
							</div>
						`)
						for(let i = 0; i<5; i++){
							let a = $('.single-product').last().find('.astxayingnahatmansistem')
							if(i < e.gnahatakan){
								$(`<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>`).appendTo(a);
							}
							else{
								$(`<i class="fa fa-star" aria-hidden="true" style="font-size: 15px"></i>`).appendTo(a)
							}
						}
					}
				})
		}
	})
});
	// $('.dirincevent').click(function(event) {
	// 	event.stopPropagation()
	// });


	//  -----------------------------------------------------compare-----------------------------------------

	let vorprodna = 0
	let firstprod
	let secprod
	$(document).on('click', '.clickforcompaere', function(event) {
		vorprodna ++
		if(vorprodna == 1){
			firstprod = $(this).attr('id')
				Swal.fire(
				  'Good job!',
				  'Now Click the second product!',
				  'success'
				)
		} 
		else if(vorprodna == 2){
			secprod = $(this).attr('id')
			vorprodna = 0
			if(secprod == firstprod){
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'This is the same product!',
					})
			}
			else{
				// $.ajax({
				// 	type:'post',
				// 	url:'/comparing',
				// 	data:{'_token':token,'first':firstprod,'sec':secprod},
				// 	success:function(r){
				// 		console.log(r)
				// 	}
				// })
				location.href = `compare/${firstprod}/${secprod}`
			}
		}
	});


});