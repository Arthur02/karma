<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Products;
use Session;
use App\Messages;
use App\Notifications;

class AdminController extends Controller
{
    public function LogIn(){
    	return view('adminhome');
    }
    public function getUsers(){
    	return Users::where('type',"!=","admin")->latest('id')->get();
    }
    public function getProducts(){
    	return Products::where('active',0)->with('category')->latest('id')->get();
    }
    public function blockUser(Request $r){
    	$id = $r->id;
    	$time = $r->time;
    	$blocktime = time() + $time*60;
    	Users::where('id',$id)->update([
    		'blocktime' => $blocktime,
    	]);
    }
    public function unblockUser(Request $r){
    	$id = $r->id;
    	Users::where('id',$id)->update([
    		'blocktime' => time(),
    	]);
    }
    public function getAllProducts(Request $r){
    	return Products::with('category')->latest('id')->get();
    }
    public function acceptproduct(Request $r){
        Products::where('id',$r->id)->update([
            'active' => 1,
        ]);
        $a = Products::where('id',$r->id)->with('user')->first();
        $not = new Notifications;
        $not->user_id = $a->user->id;
        $not->status = 'acepted';
        $not->name = $a->name;
        $not->product_id = $r->id;
        $not->save();
    }    
    public function denyproduct(Request $r){
        Products::where('id',$r->id)->update([
            'active' => 2,
        ]);
        $a = Products::where('id',$r->id)->with('user')->first();
        $not = new Notifications;
        $not->user_id = $a->user->id;
        $not->status = 'denyed';
        $not->name = $a->name;
        $not->product_id = $r->id;
        $not->save();
    }
    public function getname(Request $r){
        return Users::where('id',$r->id)->first();
    }
    public function getmessages(Request $r){
        return Messages::where('user_id',$r->id)
        ->where('admin_id', $r->myid)->orWhere('user_id',$r->myid)->where('admin_id',$r->id)->get();
    }
    public function sendmessage(Request $r){
        // if(session::has('admin')){
        //     return 'asdasdasdasd';
        // }
        // else{
        //     return 'hndushka';        
            $id = $r->id;
            $message = $r->message;
            $mes = new Messages;
            $mes->user_id = $id;
            $mes->admin_id = $r->myid;
            $mes->message = $message;
            $mes->save();
    }
}
