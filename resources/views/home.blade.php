@extends('layouts.UserLayout')
@section('title','Karma Home')
@section('content')
<!-- Start Header Area -->
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/homepage.css') }}">
</head>
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end" style="position: relative;">
			<div class="col-first aosdk" style="position: relative;">
				<h1 class="tellyourname">{{ $user['name']." ".$user['surname']  }}</h1>
				<i class="fa fa-pencil-square text-light rename" data-toggle="modal" data-target="#renameme" style="font-size: 23px;" aria-hidden="true"></i>
				<nav class="d-flex align-items-center">
					<a href="shop">Home<span class="lnr lnr-arrow-right"></span></a>
					<a href="login">Home</a>
				</nav>
			</div>
			{{-- USERIMAGE --}}
			<div class="userimage rounded-circle" style="background-image: url({{ asset("userphotos/".$user['image'] )}}) ">
				<div style="position: relative;">
					<button class="btn userimage poxnk rounded-circle"  data-toggle="modal" data-target="#changeimage"><i class="fa fa-camera camerayi"></i></button>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Banner Area -->
<!-- The Modal -->



<div class="container w-90 mx-auto" style="margin-top: 100px;">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item" style="font-size: 28px">
			<a class="nav-link genric-btn primary active" data-toggle="tab" href="#home">Cart</a>
		</li>
		<li class="nav-item" style="font-size: 28px">
			<a class="nav-link genric-btn primary" data-toggle="tab" href="#menu1">My Products</a>
		</li>
		<li class="nav-item" style="font-size: 28px">
			<a class="nav-link genric-btn primary" data-toggle="tab" href="#menu2">My Whishlist</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div id="home" class="container tab-pane active"><br>
		<div id="home" class="container tab-pane active"><br>
			<div class="w-100">
				<section style="padding-top: 20px; padding-bottom: 100px;">
		        <div class="container">
		            <div class="cart_inner">
		                <div class="table-responsive">
		                    <table class="table">
		                        <thead>
		                            <tr>
		                                <th scope="col">Product</th>
		                                <th scope="col">Price</th>
		                                <th scope="col">Quantity</th>
		                                <th scope="col">Total</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            @foreach($cart as $product)
		                            <tr style="position: relative;" class="incpetqaanpaymanjnjeq">
										
		                                <td>
		                                    <div class="media">
		                                        <div class="d-flex">
		                                            <img width="130px" src="{{asset("img/product/".$product->productimages[0]->photo) }}" alt="">
		                                        </div>
		                                        <div class="media-body">
		                                            <p>{{$product->products->name}}</p>
		                                        </div>
		                                    </div>
		                                </td>
		                                <td>
		                                    <h5>$<span class="ginnn">{{ $product->products->price }}</span>.00</h5>
		                                </td>
		                                <td>
		                                    <div class="product_count">
		                                        <input type="text" name="qty" id="sst" maxlength="12" value="{{ $product->count }}" title="Quantity:"
		                                            class="input-text qty">
		                                        <button  class="increase items-count getupman" id={{ $product->id }}>
		                                        	<i class="lnr lnr-chevron-up"></i>
		                                        </button>
		                                        <button class="reduced items-count getdownman" id="{{ $product->id }}">
		                                        	<i class="lnr lnr-chevron-down"></i>
		                                        </button>
		                                    </div>
		                                </td>
		                                <td style="position: relative;">
		                                	<p style="position: absolute; right: 10px; top: 10px; cursor: pointer;" class="removefromcart" id="{{ $product->products->id }}">&times;</p>

		                                    <h5 class="obshiginy">$720.00</h5>
		                                </td>
		                            </tr>
		                                @endforeach
		                            <tr class="bottom_button">
		                                <td>
		                                    <a class="gray_btn" href="{{ url('/allProduct') }}">Continue shop</a>
		                                </td>
		                                <td>

		                                </td>
		                                <td>

		                                </td>
		                                <td style="position: relative;">
		                                    <div class="cupon_text d-flex align-items-center" style="position: absolute;right: 10px; top: 0;bottom: 0;">
		                                        <a class="primary-btn" href="{{  url('/cart')  }}">Go to cart</a>
		                                    </div>
		                                </td>
		                            </tr>
		                            <tr>
		                                <td>

		                                </td>
		                                <td>

		                                </td>
		                                <td>
		                                    <h5>Subtotal</h5>
		                                </td>
		                                <td>
		                                    <h5 class="subsax">$2160.00</h5>
		                                </td>
		                            </tr>
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </section>
			</div>
		</div>
		</div>
		<div id="menu1" class="container tab-pane fade"><br>
			<div class="container w-90">
	<section class="lattest-product-area pb-40 category-list emptyme">
		<!-- single product -->
			<div class="row">
		@foreach($products as $product)
				<div class="col-lg-4 col-md-6">
					<div class="single-product">
						<img class="img-fluid" src="{{asset("img/product/".$product->photos[0]['photo']) }}" alt="">
							<div class="product-details">
								<h6>{{$product->name}}</h6>
								<div class="price">
									<h6>${{$product->price}}.00</h6>
									<h6 class="l-through">${{ $product->price + 50 }}.00</h6>
								</div>
								<div class="prd-bottom">
									<a class="social-info clickforcompaere" id="{{ $product->id }}" style="cursor: pointer;">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text">compare</p>
									</a>
									<a href="shop/product/{{ $product->id }}" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
			@endforeach
			</div>
	</section>

</div>
		</div>
		<div id="menu2" class="container tab-pane fade"><br>
					<div id="home" class="container tab-pane active"><br>
			<div class="w-100">
				<section style="padding-top: 20px; padding-bottom: 100px;">
		        <div class="container">
		            <div class="cart_inner">
		                <div class="table-responsive">
		                    <table class="table">
		                        <thead>
		                            <tr>
		                                <th scope="col">Product</th>
		                                <th scope="col">Price</th>
		                                <th scope="col">Move To Cart</th>
		                                <th scope="col">Delete</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            @foreach($whish as $product)
		                            <tr style="position: relative;" class="incpetqaanpaymanjnjeqwhish">
										
		                                <td>
		                                    <div class="media">
		                                        <div class="d-flex">
		                                            <img width="130px" src="{{asset("img/product/".$product->productimages[0]->photo) }}" alt="">
		                                        </div>
		                                        <div class="media-body">
		                                            <p>{{$product->products->name}}</p>
		                                        </div>
		                                    </div>
		                                </td>
		                                <td>
		                                    <h5>$<span class="ginnn">{{ $product->products->price }}</span>.00</h5>
		                                </td>
		                                <td>
											<button class="primary-btn getouttocart" style="border:none" id="{{ $product->products->id }}">Move
											</button>
											
		                                </td>
		                                <td style="position: relative;">
											<button class="primary-btn dltfromwhish" style="color: #fff; background: #f44a40; border: none"  id="{{ $product->id }}">Delete</button>
											
		                                </td>
		                            </tr>
		                                @endforeach
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </section>
			</div>
		</div>
		</div>
	</div>
</div>
</div>






<div class="modal fade" id="changeimage">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Change image</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<form method="post" action="{{ url('changeing') }}" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<input type="file" name="img" accept="image/*">
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button class="btn pbatn">Save</button>
				</div>
			</form>

		</div>
	</div>
</div>


 <div class="modal fade" id="renameme">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Rename</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        	<div class="mt-10 mb-3 ">
        		<input type="text" name="first_name" placeholder="First Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" required="" class="single-input qoanuny" value="{{ $user->name }}">
        	</div>
        	<div class="mt-10">
        		<input type="text" name="first_name" placeholder="Second Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Second Name'" required="" class="single-input qoazganuny" value="{{ $user->surname }}">
        	</div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn primary-btn stexpoxumemanuns">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

@endsection('content')