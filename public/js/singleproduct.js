jQuery(document).ready(function($) {
  let token = $('meta[name="csrf-token"]').attr('content')
  $('.dltprod').click(function(event) {
    let id = $('.dltprod').attr('id') 
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
      if (result.value) {
            // Ստեղ կուղարկես ajax
            $.ajax({
                url:'/deleteproduct',
                type:'post',
                data:{"_token":token, "id" : id},
                success:function(r){
                    window.location.href = "/myprod"
                }
            })
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
              )
        }
    })
});
  // console.log($('.asdasdasd').attr('id'))
    $(".addtokart").click(function(event) {
      if($('.asdasdasd').attr('id') == ''){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'You need to sign up for adding this to cart',
        })
      }
      else{
        let id = $(this).attr('id')
        let maxqan = $(this).attr('data_id')
        let qanak = $('.myvalisyours').val()
        if(+qanak > +maxqan || qanak<1){
          console.log(qanak)
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'You cant have this much products',
          })
        }
        else{
          $.ajax({
            type:'post',
            url:'/gotocartmyfriend',
            data:{'_token':token,'id':id,'qanak':qanak,'max':maxqan},
            success:function(r){
                    if(r != 'no'){
                      Swal.fire(
                        'Good job!',
                        'Added to cart!',
                        'success'
                      )                
                    }
                    else{
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'Something went wrong',
                      })
                    }
            }
          })
        }
      }
    });




function previewSmall(input){

    if (input.files) {
      // $('.upload').after(`<div class="small_gallery d-inline-block h-50 mb-1" style="width: 32.5%;"></div>`)
      for (let i = 0; i < input.files.length; i++){
        var reader = new FileReader();
        reader.onload = function(e){
          // console.log(e)
          $('.new-photos').append(`<div class="d-inline-block jnjirinc h-50 mb-1" style="width: 32.5%; position:relative;">
                <img class="img-fluid" style="box-sizing: border-box; padding-right:4px;" src="${e.target.result}" alt="">
            </div>`)
      }
      reader.readAsDataURL(input.files[i]);
  }
}
}

$('.image_input').change(function(){
    $('.jnjirinc').remove()
    previewSmall(this);
})

// ${e.target.result}




$('.savechanges1').click(function(event) {
    let id = $(this).attr('id')
    let name = $("#prname").val()
    let count = $("#count").val()
    let price = $("#price").val()
    let category = $("#category").val()
    let catname = $('#category').attr('data-id')
    let formData
    // console.log(catname)
    // $name = $("#name").val()
    let description = $("#description").val()

    // console.log(category)
    $.ajax({
      type:'post',
      url:'/changesingle',
      data:{"_token":token, 'name':name, 'count':count, 'price':price, 'type':category, 'description':description,  'id':id },
      success:function(r){ 

        $('.prodddddname').html(name)
        $('.prodddddprice').html('$'+price+'.00')
        $('.prodddddcount').html(count +' ' + 'left')
        $('.prodddddcategory').html(catname)
        $('.proddddddesc').html(description)


        $('.name1').removeClass('text-danger')
        $('.name1').html('Name')
        $('.price1').removeClass('text-danger')
        $('.price1').html('Price')
        $('.type1').removeClass('text-danger')
        $('.type1').html('Type')
        $('.count1').removeClass('text-danger')
        $('.count1').html('Count')
        $('.desc1').removeClass('text-danger')
        $('.desc1').html('Description')

    },
    error:function(arr){
      let a = JSON.parse(arr.responseText)
      if(typeof a['errors'] !== 'undefined'){          
        if(typeof a['errors']['name'] !=='undefined'){
          $('.name1').addClass('text-danger')
          $('.name1').html(a['errors']['name'])
      }
      if(typeof a['errors']['price'] !=='undefined'){
          $('.price1').addClass('text-danger')
          $('.price1').html(a['errors']['price'])
      }
      if(typeof a['errors']['type'] !=='undefined'){
          $('.type1').addClass('text-danger')
          $('.type1').html(a['errors']['type'])
      }
      if(typeof a['errors']['count'] !=='undefined'){
          $('.count1').addClass('text-danger')
          $('.count1').html(a['errors']['count'])
      }
      if(typeof a['errors']['description'] !=='undefined'){
          $('.desc1').addClass('text-danger')
          $('.desc1').html(a['errors']['description'])
      }
  }
} 
})
});


$('.gnaankyun').click(function(){
    let id = $(this).attr('id')
    $(this).parent().remove()
    $.ajax({
        type:'post',
        url:'/editmyphoto',
        data:{"_token":token,'id':id},
    })
})
$('.srtikqic').click(function(event) {
  let id = $(this).attr('id')
  $.ajax({
    type:'post',
    url:'/imgoingtowhishlistsinglic',
    data:{'_token':token,'id':id},
    success:function(r){
      if(r == 'karsranicaaaa'){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'You already has this product in your whishlist',
          })
      }
      else if(r == 'yesstacvecura'){
        Swal.fire({
          icon: 'success',
          title: 'Yay',
          text: 'Product added to whishlist',
        })
      }
    }
  })
});


});

// 'photo':formData,