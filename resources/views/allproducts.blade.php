@extends('layouts.UserLayout')
@section('title','Karma Shop')


@section('content')

		
{{-- @if(Session::has('key1'))
<input type="hidden" id = "search_key" value="{{Session::get('key1')}}">
@else
<input type="hidden" id = "search_key" value="">
@endif --}}
<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				<div class="col-lg-12">
					<div class="active-banner-slider owl-carousel">
						<!-- single-slide -->
						<div class="row single-slide align-items-center d-flex">
							<div class="col-lg-5 col-md-6">
								<div class="banner-content">
									<h1>Nike New <br>Collection!</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
										dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<div class="add-bag d-flex align-items-center">
										<a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="banner-img">
									<img class="img-fluid" src="{{ asset('img/banner/banner-img.png') }}" alt="">
								</div>
							</div>
						</div>
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Nike New <br>Collection!</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
										dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<div class="add-bag d-flex align-items-center">
										<a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="banner-img">
									<img class="img-fluid" src="{{ asset('img/banner/banner-img.png') }}" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- start features Area -->
	<section class="features-area section_gap">
		<div class="container">
			<div class="row features-inner">
				<!-- single features -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single-features">
						<div class="f-icon">
							<img src="{{ asset('img/features/f-icon1.png') }}" alt="">
						</div>
						<h6>Free Delivery</h6>
						<p>Free Shipping on all order</p>
					</div>
				</div>
				<!-- single features -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single-features">
						<div class="f-icon">
							<img src="{{ asset('img/features/f-icon2.png') }}" alt="">
						</div>
						<h6>Return Policy</h6>
						<p>Free Shipping on all order</p>
					</div>
				</div>
				<!-- single features -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single-features">
						<div class="f-icon">
							<img src="{{ asset('img/features/f-icon3.png') }}" alt="">
						</div>
						<h6>24/7 Support</h6>
						<p>Free Shipping on all order</p>
					</div>
				</div>
				<!-- single features -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single-features">
						<div class="f-icon">
							<img src="{{ asset('img/features/f-icon4.png') }}" alt="">
						</div>
						<h6>Secure Payment</h6>
						<p>Free Shipping on all order</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end features Area -->

	<!-- Start category Area -->
	<section class="category-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 col-md-12">
					<div class="row">
						<div class="col-lg-8 col-md-8">
							<div class="single-deal">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="{{ asset('img/category/c1.jpg') }}" alt="">
								<a href="{{ asset('img/category/c1.jpg') }}" class="img-pop-up" target="_blank">
									<div class="deal-details">
										<h6 class="deal-title">Sneaker for Sports</h6>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="single-deal">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="{{ asset('img/category/c2.jpg') }}" alt="">
								<a href="{{ asset('img/category/c2.jpg') }}" class="img-pop-up" target="_blank">
									<div class="deal-details">
										<h6 class="deal-title">Sneaker for Sports</h6>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="single-deal">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="{{ asset('img/category/c3.jpg') }}" alt="">
								<a href="{{ asset('img/category/c3.jpg') }}" class="img-pop-up" target="_blank">
									<div class="deal-details">
										<h6 class="deal-title">Product for Couple</h6>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-8 col-md-8">
							<div class="single-deal">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="{{ asset('img/category/c4.jpg') }}" alt="">
								<a href="{{ asset('img/category/c4.jpg') }}" class="img-pop-up" target="_blank">
									<div class="deal-details">
										<h6 class="deal-title">Sneaker for Sports</h6>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-deal">
						<div class="overlay"></div>
						<img class="img-fluid w-100" src="{{ asset('img/category/c5.jpg') }}" alt="">
						<a href="{{ asset('img/category/c5.jpg') }}" class="img-pop-up" target="_blank">
							<div class="deal-details">
								<h6 class="deal-title">Sneaker for Sports</h6>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End category Area -->
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-5">
{{-- 				<div class="sidebar-categories">
					<div class="head">Browse Categories</div>
					<ul class="main-categories">
						<li class="main-nav-list"><a data-toggle="collapse" href="#fruitsVegetable" aria-expanded="false" aria-controls="fruitsVegetable"><span
								 class="lnr lnr-arrow-right"></span>Men<span class="number">(53)</span></a>
						</li>

						<li class="main-nav-list"><a data-toggle="collapse" href="#meatFish" aria-expanded="false" aria-controls="meatFish"><span
								 class="lnr lnr-arrow-right"></span>Woman<span class="number">(53)</span></a>
						</li>
						<li class="main-nav-list"><a data-toggle="collapse" href="#cooking" aria-expanded="false" aria-controls="cooking"><span
								 class="lnr lnr-arrow-right"></span>Kids<span class="number">(53)</span></a>
						</li>
					</ul>
				</div> --}}
				<div class="sidebar-filter" style="margin-top:0 ">
					<div class="top-filter-head">Product Filters</div>
					<div class="common-filter">
						<div class="head">Brands</div>
						{{-- <form action="#"> --}}

						<ul>
							@foreach($category as $cat)
								<li class="filter-list clickforchangefilteraaa" data-id="{{ $cat->id }}">
									<input class="pixel-radio dirinceventaaa" type="radio" id="{{ $cat->id }}" name="brand">
									<label class="dirincevent" id="{{ $cat->id }}" for="{{ $cat->id }}">{{ $cat->name }}</label>
								</li>
							@endforeach
						</ul>
						{{-- </form> --}}
					</div>
{{-- 					<div class="common-filter">
						<div class="head">Color</div>
						<form action="#">
							<ul>
								<li class="filter-list"><input class="pixel-radio" type="radio" id="black" name="color"><label for="black">Black<span>(29)</span></label></li>
								<li class="filter-list"><input class="pixel-radio" type="radio" id="balckleather" name="color"><label for="balckleather">Black
										Leather<span>(29)</span></label></li>
								<li class="filter-list"><input class="pixel-radio" type="radio" id="blackred" name="color"><label for="blackred">Black
										with red<span>(19)</span></label></li>
								<li class="filter-list"><input class="pixel-radio" type="radio" id="gold" name="color"><label for="gold">Gold<span>(19)</span></label></li>
								<li class="filter-list"><input class="pixel-radio" type="radio" id="spacegrey" name="color"><label for="spacegrey">Spacegrey<span>(19)</span></label></li>
							</ul>
						</form>
					</div> --}}
					<div class="common-filter">
						<div class="head">Price</div>
						<div class="price-range-area">
							<div id="price-range"></div>
							<div class="value-wrapper d-flex">
								<div class="price">Price:</div>
								<span>$</span>
								<div id="lower-value"></div>
								<div class="to">to</div>
								<span>$</span>
								<div id="upper-value"></div>
							</div>
						</div>
						<button class="primary-btn mt-3 ml-5 esfiltrumem" style="border:none">Confirm Filter</button>

					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8 col-md-7">
				<!-- Start Filter Bar -->
					<div class="filter-bar d-flex flex-wrap align-items-center">

							{{-- <div class="pagination"> --}}
								{{ $products->links() }}
							{{-- </div> --}}
						</div>
				<!-- End Filter Bar -->
				<!-- Start Best Seller -->
	<section class="lattest-product-area pb-40 category-list sranqpraducten">
		<!-- single product -->
			<div class="row sraaaaaaaaaaaaaaaaaaan">
				{{-- {{ dd($products) }} --}}
		@foreach($products as $product)
			@if($product->count != 0)
				<div class="col-lg-4 col-md-6">
					<div class="single-product">
						<div style="position: relative;">
							<img class="img-fluid" src="{{ asset("img/product/".$product->photos[0]['photo']) }}" alt="">
								<div style="position: absolute;bottom: 23px; right: 10px;">
									@for($i = 0 ; $i<5; $i++)
										@if($i < $product->gnahatakan )
										<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>
										@else
										<i class="fa fa-star" aria-hidden="true" style="font-size: 15px"></i>
										@endif
									@endfor
								</div>
						</div>
							<div class="product-details">
								<h6>{{$product->name}}</h6>
								<div class="price">
									<h6>${{$product->price}}.00</h6>
									<h6 class="l-through">${{ $product->price +50 }}.00</h6>
								</div>
								<div class="prd-bottom">

									<a class="social-info clickforbuy" style="cursor:pointer" id="{{ $product->id }}">
										<span class="ti-bag"></span>
										<p class="hover-text">add to bag</p>
									</a>
									<a class="social-info clickforwhish" style="cursor:pointer" id="{{ $product->id }}">
										<span class="lnr lnr-heart"></span>
										<p class="hover-text">Wishlist</p>
									</a>
									<a class="social-info clickforcompaere" style="cursor:pointer" id="{{ $product->id }}">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text">compare</p>
									</a>
									<a href="{{ asset('shop/product/'.$product->id) }}" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
					@endif
			@endforeach
			</div>
	</section>
				<!-- End Best Seller -->
				<!-- Start Filter Bar -->
		{{-- <div class="container mb-5"> --}}
			<div class="filter-bar d-flex flex-wrap align-items-center mb-5">
					{{-- <div class="pagination"> --}}
						{{ $products->links() }}
					{{-- </div> --}}
				</div>
		{{-- </div> --}}
				<!-- End Filter Bar -->
			</div>
		</div>
	</div>
	<!-- start product Area -->



@endsection('content')



