/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100411
Source Host           : localhost:3306
Source Database       : online_shop

Target Server Type    : MYSQL
Target Server Version : 100411
File Encoding         : 65001

Date: 2020-08-04 12:05:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_product_id_foreign` (`product_id`),
  KEY `cart_user_id_foreign` (`user_id`),
  CONSTRAINT `cart_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('57', '1', '42', '21', '2020-07-31 14:35:44', '2020-07-31 14:35:44');
INSERT INTO `cart` VALUES ('60', '1', '26', '1', '2020-08-02 19:42:16', '2020-08-02 19:42:16');
INSERT INTO `cart` VALUES ('61', '1', '11', '1', '2020-08-03 17:01:34', '2020-08-03 17:01:34');
INSERT INTO `cart` VALUES ('63', '1', '43', '25', '2020-08-04 01:15:01', '2020-08-04 01:15:01');
INSERT INTO `cart` VALUES ('69', '2', '45', '3', '2020-08-04 01:39:29', '2020-08-04 01:39:32');
INSERT INTO `cart` VALUES ('73', '1', '13', '27', '2020-08-04 09:39:08', '2020-08-04 09:39:08');
INSERT INTO `cart` VALUES ('74', '1', '39', '27', '2020-08-04 09:39:58', '2020-08-04 09:39:58');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('7', 'Nike', null, null);
INSERT INTO `category` VALUES ('8', 'Adidas', null, null);
INSERT INTO `category` VALUES ('9', 'Vans', null, null);
INSERT INTO `category` VALUES ('10', 'Converse', null, null);
INSERT INTO `category` VALUES ('11', 'New Balance', null, null);
INSERT INTO `category` VALUES ('12', 'Skechers', null, null);
INSERT INTO `category` VALUES ('13', 'Brooks', null, null);
INSERT INTO `category` VALUES ('14', 'Puma', null, null);
INSERT INTO `category` VALUES ('15', 'Dr.Scholl\'s', null, null);
INSERT INTO `category` VALUES ('16', 'Steve Madden', null, null);

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `messages_admin_id_foreign` (`admin_id`),
  KEY `messages_user_id_foreign` (`user_id`),
  CONSTRAINT `messages_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', '21', '23', 'asdasd', '2020-08-03 18:32:55', '2020-08-03 18:32:55', '0');
INSERT INTO `messages` VALUES ('4', '23', '21', 'alo', '0000-00-00 00:00:00', '2020-08-03 18:32:55', '0');
INSERT INTO `messages` VALUES ('5', '23', '21', 'alo', '2020-08-03 18:54:10', '2020-08-03 18:54:10', '0');
INSERT INTO `messages` VALUES ('6', '23', '21', 'stugum', '2020-08-03 18:54:58', '2020-08-03 18:54:58', '0');
INSERT INTO `messages` VALUES ('7', '23', '21', 'ok', '2020-08-03 18:56:42', '2020-08-03 18:56:42', '0');
INSERT INTO `messages` VALUES ('8', '23', '24', 'yo', '2020-08-03 19:48:50', '2020-08-03 19:48:50', '0');
INSERT INTO `messages` VALUES ('9', '1', '21', 'alo', '2020-08-03 20:10:52', '2020-08-03 20:10:52', '0');
INSERT INTO `messages` VALUES ('10', '1', '21', 'lsvuma?', '2020-08-03 20:11:18', '2020-08-03 20:11:18', '0');
INSERT INTO `messages` VALUES ('11', '23', '1', 'Alo lsum es?', '2020-08-03 20:12:31', '2020-08-04 01:47:50', '1');
INSERT INTO `messages` VALUES ('12', '1', '23', 'Ha', '2020-08-03 20:12:48', '2020-08-03 20:12:48', '0');
INSERT INTO `messages` VALUES ('13', '1', '23', 'asdasd', '2020-08-03 20:21:45', '2020-08-03 20:21:45', '0');
INSERT INTO `messages` VALUES ('14', '23', '1', 'dfghjk', '2020-08-03 20:34:48', '2020-08-04 01:47:50', '1');
INSERT INTO `messages` VALUES ('15', '23', '1', 'mq', '2020-08-03 21:37:46', '2020-08-04 01:47:50', '1');
INSERT INTO `messages` VALUES ('16', '1', '23', 'Alo', '2020-08-03 21:40:00', '2020-08-03 21:40:00', '0');
INSERT INTO `messages` VALUES ('17', '23', '24', 'test', '2020-08-03 22:47:29', '2020-08-03 22:47:29', '0');
INSERT INTO `messages` VALUES ('18', '23', '1', 'lsum es', '2020-08-03 22:47:39', '2020-08-04 01:47:50', '1');
INSERT INTO `messages` VALUES ('19', '1', '23', 'ha lsvuma', '2020-08-03 22:48:09', '2020-08-03 22:48:09', '0');
INSERT INTO `messages` VALUES ('20', '23', '1', 'ok', '2020-08-04 01:23:11', '2020-08-04 01:47:50', '1');
INSERT INTO `messages` VALUES ('21', '27', '23', 'klm;,l\';', '2020-08-04 09:41:03', '2020-08-04 09:41:03', '0');
INSERT INTO `messages` VALUES ('22', '23', '27', ';lklk', '2020-08-04 09:41:34', '2020-08-04 09:43:53', '1');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('39', '2020_06_26_151541_create_users_table', '1');
INSERT INTO `migrations` VALUES ('40', '2020_07_03_185304_create_products_table', '1');
INSERT INTO `migrations` VALUES ('41', '2020_07_05_195135_create_product_photos_table', '1');
INSERT INTO `migrations` VALUES ('42', '2020_07_06_173824_create_categories_table', '1');
INSERT INTO `migrations` VALUES ('43', '2020_07_15_143946_create_carts_table', '1');
INSERT INTO `migrations` VALUES ('44', '2020_07_15_144748_create_whishlists_table', '1');
INSERT INTO `migrations` VALUES ('45', '2020_07_22_183331_create_orders_table', '1');
INSERT INTO `migrations` VALUES ('46', '2020_07_22_184516_create_orderdetales_table', '1');
INSERT INTO `migrations` VALUES ('47', '2020_07_25_210910_create_ratings_table', '1');
INSERT INTO `migrations` VALUES ('48', '2020_08_03_122944_create_messages_table', '1');
INSERT INTO `migrations` VALUES ('49', '2020_08_03_163835_create_notifications_table', '2');

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `seen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_foreign` (`user_id`),
  CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES ('2', '1', 'denyed', 'Producthamar 1333333', '2020-08-03 17:08:46', '2020-08-04 01:49:26', '42', '1');
INSERT INTO `notifications` VALUES ('3', '1', 'denyed', 'New prod', '2020-08-03 17:08:47', '2020-08-04 01:49:26', '41', '1');
INSERT INTO `notifications` VALUES ('4', '1', 'acepted', 'Nikes', '2020-08-03 19:03:40', '2020-08-04 01:49:26', '43', '1');
INSERT INTO `notifications` VALUES ('5', '1', 'acepted', 'Producthamar 1333333', '2020-08-03 22:49:22', '2020-08-04 01:49:26', '42', '1');
INSERT INTO `notifications` VALUES ('6', '1', 'denyed', 'New prod', '2020-08-03 22:49:24', '2020-08-04 01:49:26', '41', '1');
INSERT INTO `notifications` VALUES ('7', '25', 'acepted', 'EStaza prdaaskfajsl', '2020-08-04 01:24:32', '2020-08-04 01:25:50', '45', '1');
INSERT INTO `notifications` VALUES ('8', '25', 'denyed', 'EStaza prdaaskfajsl', '2020-08-04 01:24:53', '2020-08-04 01:25:50', '45', '1');
INSERT INTO `notifications` VALUES ('9', '25', 'acepted', 'EStaza prdaaskfajsl', '2020-08-04 01:25:00', '2020-08-04 01:25:50', '45', '1');
INSERT INTO `notifications` VALUES ('10', '1', 'acepted', 'New proda', '2020-08-04 01:49:20', '2020-08-04 01:49:26', '41', '1');
INSERT INTO `notifications` VALUES ('11', '27', 'acepted', 'asdasd', '2020-08-04 09:42:39', '2020-08-04 09:43:23', '46', '1');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `total` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_user_id_foreign` (`user_id`),
  CONSTRAINT `order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('8', '1', '77175', '2020-07-23 17:53:19', '2020-07-23 17:53:19', '2020-07-23 17:53:19');
INSERT INTO `order` VALUES ('10', '1', '915', '2020-07-23 18:48:00', '2020-07-23 18:48:00', '2020-07-23 18:48:00');
INSERT INTO `order` VALUES ('13', '1', '915', '2020-07-23 18:50:02', '2020-07-23 18:50:02', '2020-07-23 18:50:02');
INSERT INTO `order` VALUES ('25', '1', '915', '2020-07-23 19:17:52', '2020-07-23 19:17:52', '2020-07-23 19:17:52');
INSERT INTO `order` VALUES ('31', '1', '9915', '2020-07-25 15:24:31', '2020-07-25 15:24:31', '2020-07-25 15:24:31');
INSERT INTO `order` VALUES ('36', '1', '5800', '2020-07-25 17:43:55', '2020-07-25 17:43:55', '2020-07-25 17:43:55');
INSERT INTO `order` VALUES ('37', '21', '681', '2020-07-28 12:47:14', '2020-07-28 12:47:14', '2020-07-28 12:47:14');
INSERT INTO `order` VALUES ('38', '22', '681', '2020-07-28 12:56:39', '2020-07-28 12:56:39', '2020-07-28 12:56:39');
INSERT INTO `order` VALUES ('39', '1', '930', '2020-07-28 16:17:16', '2020-07-28 16:17:16', '2020-07-28 16:17:16');
INSERT INTO `order` VALUES ('40', '1', '7360', '2020-07-29 17:39:18', '2020-07-29 17:39:18', '2020-07-29 17:39:18');
INSERT INTO `order` VALUES ('41', '1', '271800', '2020-07-30 14:52:01', '2020-07-30 14:52:01', '2020-07-30 14:52:01');
INSERT INTO `order` VALUES ('42', '1', '450', '2020-07-30 21:12:24', '2020-07-30 21:12:24', '2020-07-30 21:12:24');
INSERT INTO `order` VALUES ('43', '1', '600', '2020-07-30 21:29:29', '2020-07-30 21:29:29', '2020-07-30 21:29:29');
INSERT INTO `order` VALUES ('44', '1', '1395', '2020-07-30 21:56:44', '2020-07-30 21:56:44', '2020-07-30 21:56:44');
INSERT INTO `order` VALUES ('45', '21', '4480', '2020-07-31 14:00:43', '2020-07-31 14:00:43', '2020-07-31 14:00:43');
INSERT INTO `order` VALUES ('46', '1', '918', '2020-08-02 19:33:18', '2020-08-02 19:33:18', '2020-08-02 19:33:18');
INSERT INTO `order` VALUES ('47', '1', '71604', '2020-08-02 19:37:17', '2020-08-02 19:37:17', '2020-08-02 19:37:17');
INSERT INTO `order` VALUES ('48', '25', '384', '2020-08-04 01:14:32', '2020-08-04 01:14:32', '2020-08-04 01:14:32');
INSERT INTO `order` VALUES ('49', '26', '320', '2020-08-04 01:19:55', '2020-08-04 01:19:55', '2020-08-04 01:19:55');
INSERT INTO `order` VALUES ('50', '3', '160', '2020-08-04 01:27:46', '2020-08-04 01:27:46', '2020-08-04 01:27:46');
INSERT INTO `order` VALUES ('51', '3', '32', '2020-08-04 01:36:23', '2020-08-04 01:36:23', '2020-08-04 01:36:23');
INSERT INTO `order` VALUES ('52', '3', '32', '2020-08-04 01:37:55', '2020-08-04 01:37:55', '2020-08-04 01:37:55');
INSERT INTO `order` VALUES ('53', '3', '1152', '2020-08-04 01:38:40', '2020-08-04 01:38:40', '2020-08-04 01:38:40');
INSERT INTO `order` VALUES ('54', '27', '507', '2020-08-04 09:37:33', '2020-08-04 09:37:33', '2020-08-04 09:37:33');

-- ----------------------------
-- Table structure for orderdetales
-- ----------------------------
DROP TABLE IF EXISTS `orderdetales`;
CREATE TABLE `orderdetales` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderdetales_order_id_foreign` (`order_id`),
  KEY `orderdetales_product_id_foreign` (`product_id`),
  CONSTRAINT `orderdetales_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `orderdetales_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of orderdetales
-- ----------------------------
INSERT INTO `orderdetales` VALUES ('13', '4', '8', '459', '2020-07-23 17:53:19', '2020-07-23 17:53:19');
INSERT INTO `orderdetales` VALUES ('14', '26', '8', '5', '2020-07-23 17:53:19', '2020-07-23 17:53:19');
INSERT INTO `orderdetales` VALUES ('15', '13', '8', '1', '2020-07-23 17:53:19', '2020-07-23 17:53:19');
INSERT INTO `orderdetales` VALUES ('16', '14', '8', '8', '2020-07-23 17:53:19', '2020-07-23 17:53:19');
INSERT INTO `orderdetales` VALUES ('17', '4', '10', '1', '2020-07-23 18:48:00', '2020-07-23 18:48:00');
INSERT INTO `orderdetales` VALUES ('18', '11', '10', '1', '2020-07-23 18:48:00', '2020-07-23 18:48:00');
INSERT INTO `orderdetales` VALUES ('19', '12', '10', '1', '2020-07-23 18:48:00', '2020-07-23 18:48:00');
INSERT INTO `orderdetales` VALUES ('20', '4', '13', '1', '2020-07-23 18:50:02', '2020-07-23 18:50:02');
INSERT INTO `orderdetales` VALUES ('21', '11', '13', '1', '2020-07-23 18:50:02', '2020-07-23 18:50:02');
INSERT INTO `orderdetales` VALUES ('22', '12', '13', '1', '2020-07-23 18:50:02', '2020-07-23 18:50:02');
INSERT INTO `orderdetales` VALUES ('28', '4', '25', '1', '2020-07-23 19:17:52', '2020-07-23 19:17:52');
INSERT INTO `orderdetales` VALUES ('29', '11', '25', '1', '2020-07-23 19:17:52', '2020-07-23 19:17:52');
INSERT INTO `orderdetales` VALUES ('30', '12', '25', '1', '2020-07-23 19:17:52', '2020-07-23 19:17:52');
INSERT INTO `orderdetales` VALUES ('41', '4', '31', '1', '2020-07-25 15:24:31', '2020-07-25 15:24:31');
INSERT INTO `orderdetales` VALUES ('42', '11', '31', '21', '2020-07-25 15:24:31', '2020-07-25 15:24:31');
INSERT INTO `orderdetales` VALUES ('43', '12', '31', '1', '2020-07-25 15:24:31', '2020-07-25 15:24:31');
INSERT INTO `orderdetales` VALUES ('47', '4', '36', '10', '2020-07-25 17:43:55', '2020-07-25 17:43:55');
INSERT INTO `orderdetales` VALUES ('48', '11', '36', '6', '2020-07-25 17:43:55', '2020-07-25 17:43:55');
INSERT INTO `orderdetales` VALUES ('49', '12', '36', '5', '2020-07-25 17:43:55', '2020-07-25 17:43:55');
INSERT INTO `orderdetales` VALUES ('50', '1', '37', '1', '2020-07-28 12:47:14', '2020-07-28 12:47:14');
INSERT INTO `orderdetales` VALUES ('51', '2', '37', '1', '2020-07-28 12:47:14', '2020-07-28 12:47:14');
INSERT INTO `orderdetales` VALUES ('52', '3', '37', '1', '2020-07-28 12:47:14', '2020-07-28 12:47:14');
INSERT INTO `orderdetales` VALUES ('53', '1', '38', '1', '2020-07-28 12:56:39', '2020-07-28 12:56:39');
INSERT INTO `orderdetales` VALUES ('54', '2', '38', '1', '2020-07-28 12:56:39', '2020-07-28 12:56:39');
INSERT INTO `orderdetales` VALUES ('55', '3', '38', '1', '2020-07-28 12:56:39', '2020-07-28 12:56:39');
INSERT INTO `orderdetales` VALUES ('56', '4', '39', '6', '2020-07-28 16:17:16', '2020-07-28 16:17:16');
INSERT INTO `orderdetales` VALUES ('57', '4', '40', '2', '2020-07-29 17:39:18', '2020-07-29 17:39:18');
INSERT INTO `orderdetales` VALUES ('58', '11', '40', '1', '2020-07-29 17:39:18', '2020-07-29 17:39:18');
INSERT INTO `orderdetales` VALUES ('59', '14', '40', '11', '2020-07-29 17:39:18', '2020-07-29 17:39:18');
INSERT INTO `orderdetales` VALUES ('60', '14', '41', '453', '2020-07-30 14:52:01', '2020-07-30 14:52:01');
INSERT INTO `orderdetales` VALUES ('61', '11', '42', '1', '2020-07-30 21:12:24', '2020-07-30 21:12:24');
INSERT INTO `orderdetales` VALUES ('62', '14', '43', '1', '2020-07-30 21:29:29', '2020-07-30 21:29:29');
INSERT INTO `orderdetales` VALUES ('63', '4', '44', '9', '2020-07-30 21:56:44', '2020-07-30 21:56:44');
INSERT INTO `orderdetales` VALUES ('64', '42', '45', '14', '2020-07-31 14:00:43', '2020-07-31 14:00:43');
INSERT INTO `orderdetales` VALUES ('65', '26', '46', '3', '2020-08-02 19:33:18', '2020-08-02 19:33:18');
INSERT INTO `orderdetales` VALUES ('66', '13', '46', '1', '2020-08-02 19:33:18', '2020-08-02 19:33:18');
INSERT INTO `orderdetales` VALUES ('67', '26', '47', '459', '2020-08-02 19:37:17', '2020-08-02 19:37:17');
INSERT INTO `orderdetales` VALUES ('68', '43', '48', '12', '2020-08-04 01:14:32', '2020-08-04 01:14:32');
INSERT INTO `orderdetales` VALUES ('69', '42', '49', '1', '2020-08-04 01:19:55', '2020-08-04 01:19:55');
INSERT INTO `orderdetales` VALUES ('70', '45', '50', '5', '2020-08-04 01:27:46', '2020-08-04 01:27:46');
INSERT INTO `orderdetales` VALUES ('71', '45', '51', '1', '2020-08-04 01:36:23', '2020-08-04 01:36:23');
INSERT INTO `orderdetales` VALUES ('72', '45', '52', '1', '2020-08-04 01:37:55', '2020-08-04 01:37:55');
INSERT INTO `orderdetales` VALUES ('73', '45', '53', '36', '2020-08-04 01:38:40', '2020-08-04 01:38:40');
INSERT INTO `orderdetales` VALUES ('74', '45', '54', '2', '2020-08-04 09:37:33', '2020-08-04 09:37:33');
INSERT INTO `orderdetales` VALUES ('75', '42', '54', '1', '2020-08-04 09:37:33', '2020-08-04 09:37:33');
INSERT INTO `orderdetales` VALUES ('76', '41', '54', '1', '2020-08-04 09:37:33', '2020-08-04 09:37:33');

-- ----------------------------
-- Table structure for productphotos
-- ----------------------------
DROP TABLE IF EXISTS `productphotos`;
CREATE TABLE `productphotos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productphotos_product_id_foreign` (`product_id`),
  CONSTRAINT `productphotos_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of productphotos
-- ----------------------------
INSERT INTO `productphotos` VALUES ('2', '1594218724p8.jpg', '2', '2020-07-08 18:32:04', '2020-07-08 18:32:04');
INSERT INTO `productphotos` VALUES ('3', '1594479449p1.jpg', '3', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('4', '1594479449p2.jpg', '4', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('5', '1594479449p7.jpg', '5', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('6', '1594479449p8.jpg', '6', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('7', '1594479546p6.jpg', '7', '2020-07-11 18:59:06', '2020-07-11 18:59:06');
INSERT INTO `productphotos` VALUES ('8', '1594479571p3.jpg', '8', '2020-07-11 18:59:31', '2020-07-11 18:59:31');
INSERT INTO `productphotos` VALUES ('9', '1594479449p8.jpg', '9', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('10', '1594479546p6.jpg', '10', '2020-07-11 18:59:06', '2020-07-11 18:59:06');
INSERT INTO `productphotos` VALUES ('11', '1594479571p3.jpg', '1', '2020-07-11 18:59:31', '2020-07-11 18:59:31');
INSERT INTO `productphotos` VALUES ('13', '1594820535p4.jpg', '11', '2020-07-15 17:42:15', '2020-07-15 17:42:15');
INSERT INTO `productphotos` VALUES ('14', '1594820535p5.jpg', '12', '2020-07-15 17:42:15', '2020-07-15 17:42:15');
INSERT INTO `productphotos` VALUES ('15', '1594820535p6.jpg', '13', '2020-07-15 17:42:16', '2020-07-15 17:42:16');
INSERT INTO `productphotos` VALUES ('16', '1594820536p7.jpg', '14', '2020-07-15 17:42:16', '2020-07-15 17:42:16');
INSERT INTO `productphotos` VALUES ('35', '1594218724p7.jpg', '26', '2020-07-08 18:32:04', '2020-07-08 18:32:04');
INSERT INTO `productphotos` VALUES ('36', '1594218724p8.jpg', '26', '2020-07-08 18:32:04', '2020-07-08 18:32:04');
INSERT INTO `productphotos` VALUES ('59', '1594479449p1.jpg', '38', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('60', '1594479449p2.jpg', '38', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('61', '1594479449p7.jpg', '38', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('62', '1594479449p8.jpg', '38', '2020-07-11 18:57:29', '2020-07-11 18:57:29');
INSERT INTO `productphotos` VALUES ('63', '1594479546p6.jpg', '39', '2020-07-11 18:59:06', '2020-07-11 18:59:06');
INSERT INTO `productphotos` VALUES ('64', '1594479571p3.jpg', '40', '2020-07-11 18:59:31', '2020-07-11 18:59:31');
INSERT INTO `productphotos` VALUES ('65', '1594479813p3.jpg', '41', '2020-07-11 19:03:33', '2020-07-11 19:03:33');
INSERT INTO `productphotos` VALUES ('66', '1594479813p4.jpg', '41', '2020-07-11 19:03:33', '2020-07-11 19:03:33');
INSERT INTO `productphotos` VALUES ('67', '1594479813p5.jpg', '41', '2020-07-11 19:03:33', '2020-07-11 19:03:33');
INSERT INTO `productphotos` VALUES ('68', '1594479813p6.jpg', '41', '2020-07-11 19:03:33', '2020-07-11 19:03:33');
INSERT INTO `productphotos` VALUES ('71', '1594803441p4.jpg', '1', '2020-07-15 12:57:21', '2020-07-15 12:57:21');
INSERT INTO `productphotos` VALUES ('72', '1594803441p5.jpg', '1', '2020-07-15 12:57:21', '2020-07-15 12:57:21');
INSERT INTO `productphotos` VALUES ('73', '1594803441p6.jpg', '1', '2020-07-15 12:57:21', '2020-07-15 12:57:21');
INSERT INTO `productphotos` VALUES ('74', '1594803441p7.jpg', '1', '2020-07-15 12:57:21', '2020-07-15 12:57:21');
INSERT INTO `productphotos` VALUES ('95', '1594820535p4.jpg', '2', '2020-07-15 17:42:15', '2020-07-15 17:42:15');
INSERT INTO `productphotos` VALUES ('96', '1594820535p5.jpg', '2', '2020-07-15 17:42:15', '2020-07-15 17:42:15');
INSERT INTO `productphotos` VALUES ('97', '1594820535p6.jpg', '2', '2020-07-15 17:42:16', '2020-07-15 17:42:16');
INSERT INTO `productphotos` VALUES ('98', '1594820536p7.jpg', '2', '2020-07-15 17:42:16', '2020-07-15 17:42:16');
INSERT INTO `productphotos` VALUES ('99', '1595513832p3.jpg', '42', '2020-07-23 18:17:12', '2020-07-23 18:17:12');
INSERT INTO `productphotos` VALUES ('100', '1595513832p4.jpg', '42', '2020-07-23 18:17:12', '2020-07-23 18:17:12');
INSERT INTO `productphotos` VALUES ('101', '1595513832p5.jpg', '42', '2020-07-23 18:17:12', '2020-07-23 18:17:12');
INSERT INTO `productphotos` VALUES ('102', '1595513898p5.jpg', '42', '2020-07-23 18:18:18', '2020-07-23 18:18:18');
INSERT INTO `productphotos` VALUES ('104', '1596386877p2.jpg', '41', '2020-08-02 20:47:57', '2020-08-02 20:47:57');
INSERT INTO `productphotos` VALUES ('105', '1596466982p1.jpg', '43', '2020-08-03 19:03:02', '2020-08-03 19:03:02');
INSERT INTO `productphotos` VALUES ('111', '1596488837p5.jpg', '45', '2020-08-04 01:07:17', '2020-08-04 01:07:17');
INSERT INTO `productphotos` VALUES ('112', '1596488837p6.jpg', '45', '2020-08-04 01:07:17', '2020-08-04 01:07:17');
INSERT INTO `productphotos` VALUES ('113', '1596488837p7.jpg', '45', '2020-08-04 01:07:17', '2020-08-04 01:07:17');
INSERT INTO `productphotos` VALUES ('114', '1596488837p8.jpg', '45', '2020-08-04 01:07:17', '2020-08-04 01:07:17');
INSERT INTO `productphotos` VALUES ('115', '1596519286p5.jpg', '46', '2020-08-04 09:34:46', '2020-08-04 09:34:46');
INSERT INTO `productphotos` VALUES ('117', '1596519286p7.jpg', '46', '2020-08-04 09:34:46', '2020-08-04 09:34:46');
INSERT INTO `productphotos` VALUES ('118', '1596519286p8.jpg', '46', '2020-08-04 09:34:46', '2020-08-04 09:34:46');
INSERT INTO `productphotos` VALUES ('119', '1596519355p6.jpg', '46', '2020-08-04 09:35:55', '2020-08-04 09:35:55');
INSERT INTO `productphotos` VALUES ('120', '1596519355p7.jpg', '46', '2020-08-04 09:35:55', '2020-08-04 09:35:55');
INSERT INTO `productphotos` VALUES ('121', '1596519355p8.jpg', '46', '2020-08-04 09:35:55', '2020-08-04 09:35:55');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `products_user_id_foreign` (`user_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', '150', '498', 'նյու կոշիկներ, շատ լավն են, խոսքի նենց լավն են, որ ես ասում եմ էս ինչ լավն են, ասեմ ավելին իրանք ավելի շատ բոթաս են քան կոշիկ', '1', '9', '2020-07-11 18:57:29', '2020-07-28 12:56:39', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('2', '210', '43', 'Ռոզվի բոթաս', '1', '8', '2020-07-11 18:59:06', '2020-08-02 19:26:01', 'aaa', '1');
INSERT INTO `products` VALUES ('3', '321', '43', 'Սև skechers', '1', '12', '2020-07-11 18:59:31', '2020-08-02 19:25:38', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('4', '155', '425', 'asd', '3', '8', '2020-07-08 18:32:04', '2020-08-02 19:23:34', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '2');
INSERT INTO `products` VALUES ('5', '150', '500', 'նյու կոշիկներ, շատ լավն են, խոսքի նենց լավն են, որ ես ասում եմ էս ինչ լավն են, ասեմ ավելին իրանք ավելի շատ բոթաս են քան կոշիկ', '1', '9', '2020-07-11 18:57:29', '2020-08-02 19:23:21', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '2');
INSERT INTO `products` VALUES ('6', '150', '5', 'Կարմիր գույնի բոթաս', '1', '11', '2020-07-11 18:59:06', '2020-08-02 19:23:13', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '2');
INSERT INTO `products` VALUES ('7', '321', '45', 'Սև skechers', '1', '12', '2020-07-11 18:59:31', '2020-08-02 19:23:00', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '2');
INSERT INTO `products` VALUES ('8', '150', '500', 'նյու կոշիկներ, շատ լավն են, խոսքի նենց լավն են, որ ես ասում եմ էս ինչ լավն են, ասեմ ավելին իրանք ավելի շատ բոթաս են քան կոշիկ', '1', '9', '2020-07-11 18:57:29', '2020-08-02 19:22:44', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('9', '150', '5', 'Կարմիր գույնի բոթաս', '1', '11', '2020-07-11 18:59:06', '2020-08-02 19:16:54', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('10', '321', '45', 'Սև skechers', '1', '12', '2020-07-11 18:59:31', '2020-08-02 19:16:24', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('11', '450', '445', 'ban1', '3', '8', '2020-07-08 18:32:04', '2020-08-02 19:15:59', 'ban1', '1');
INSERT INTO `products` VALUES ('12', '310', '448', 'ban2', '3', '10', '2020-07-08 18:32:04', '2020-08-02 19:14:55', 'ban2', '1');
INSERT INTO `products` VALUES ('13', '450', '463', 'ban3', '3', '8', '2020-07-08 18:32:04', '2020-08-02 19:33:18', 'ban3', '1');
INSERT INTO `products` VALUES ('14', '600', '19', 'ban4', '3', '10', '2020-07-08 18:32:04', '2020-08-02 19:13:22', 'ban4', '1');
INSERT INTO `products` VALUES ('26', '156', '2', 'asd', '3', '10', '2020-07-08 18:32:04', '2020-08-02 19:37:17', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('38', '150', '500', 'նյու կոշիկներ, շատ լավն են, խոսքի նենց լավն են, որ ես ասում եմ էս ինչ լավն են, ասեմ ավելին իրանք ավելի շատ բոթաս են քան կոշիկ', '1', '9', '2020-07-11 18:57:29', '2020-08-02 19:10:38', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('39', '150', '5', 'Կարմիր գույնի բոթաս', '1', '11', '2020-07-11 18:59:06', '2020-08-03 16:51:12', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '1');
INSERT INTO `products` VALUES ('40', '321', '45', 'Սև skechers', '1', '12', '2020-07-11 18:59:31', '2020-08-01 21:55:07', 'ADDIDAS NEW HAMMER SOLE FOR SPORTS PERSON', '2');
INSERT INTO `products` VALUES ('41', '123', '34', 'asdasdasd', '1', '10', '2020-07-11 19:03:33', '2020-08-04 09:37:33', 'New proda', '1');
INSERT INTO `products` VALUES ('42', '320', '440', 'its my product', '1', '8', '2020-07-23 18:17:12', '2020-08-04 09:37:33', 'Producthamar 1333333', '1');
INSERT INTO `products` VALUES ('43', '32', '0', 'Yay its nikes', '1', '7', '2020-08-03 19:03:02', '2020-08-04 01:14:32', 'Nikes', '1');
INSERT INTO `products` VALUES ('45', '32', '0', 'asdsad', '25', '14', '2020-08-04 01:07:17', '2020-08-04 09:37:33', 'EStaza prdaaskfajsl', '1');
INSERT INTO `products` VALUES ('46', '210', '45', 'tryguyhjlk;l', '27', '13', '2020-08-04 09:34:46', '2020-08-04 09:42:39', 'asdasd', '1');

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_user_id_foreign` (`user_id`),
  KEY `ratings_product_id_foreign` (`product_id`),
  CONSTRAINT `ratings_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of ratings
-- ----------------------------
INSERT INTO `ratings` VALUES ('1', '4', '1', '4', 'Norm', '2020-07-26 17:29:32', '2020-07-30 21:57:17');
INSERT INTO `ratings` VALUES ('2', '1', '21', '1', 'vatna nenc vatna vor vabshe durs chekav, misht es xanutic em arnum misht shat lavn en exe bayc henc es meky shat vatna', '2020-07-28 12:48:19', '2020-07-28 12:53:23');
INSERT INTO `ratings` VALUES ('4', '2', '21', '5', 'Lavna', '2020-07-28 12:53:35', '2020-07-28 12:53:35');
INSERT INTO `ratings` VALUES ('5', '1', '22', '3', 'message', '2020-07-28 12:57:07', '2020-07-28 12:57:07');
INSERT INTO `ratings` VALUES ('6', '14', '1', '5', 'Shat lav bana', '2020-07-30 21:29:58', '2020-07-30 21:29:58');
INSERT INTO `ratings` VALUES ('7', '42', '21', '5', 'Shat lav praducta aravel evs ete hashvi arnenq vor inqy hamar 1 a', '2020-07-31 14:01:12', '2020-07-31 14:01:12');
INSERT INTO `ratings` VALUES ('8', '26', '1', '4', 'Շատ լավ ապրանքա փայց ամեն դեպքում մինուսներ կան, եթե ուղղվա նորից կա', '2020-08-02 19:36:04', '2020-08-02 19:36:04');
INSERT INTO `ratings` VALUES ('9', '42', '26', '3', 'Normala N13333 i hamar', '2020-08-04 01:20:17', '2020-08-04 01:20:17');
INSERT INTO `ratings` VALUES ('10', '45', '3', '5', 'fasdasd', '2020-08-04 01:31:09', '2020-08-04 01:34:32');
INSERT INTO `ratings` VALUES ('11', '45', '27', '4', 'asdasd', '2020-08-04 09:38:15', '2020-08-04 09:38:15');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `active` int(11) NOT NULL DEFAULT 0,
  `emailcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `blocktime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `logintime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Arthur', 'Voskanyan', '47', 'arthur-voskanyan@mail.ru', '$2y$10$gJtFIQDjvoyysEDV2.XdnOgEKBvMK7g4QHhk13k5V68hl0QAMMrlG', '2020-07-06 17:53:05', '2020-08-04 09:47:31', '5f21801ed9ca9p6.jpg', '1', '', 'user', '1596519741', '2020/08/04');
INSERT INTO `users` VALUES ('3', 'Partur', 'asd', '65', 'partur@gmail.com', '$2y$10$g9BXUFWQYqm/2mB28.dASepjM8c1yZWFjs4njQsEtbTVN2TUGRJ5S', '2020-07-08 17:53:44', '2020-08-04 01:32:55', '5f05cff8bc99b02.jpg', '1', null, 'user', '1596286678', '2020/08/04');
INSERT INTO `users` VALUES ('21', 'Arthur', 'Pharthur', '47', 'karibyanartur-00@mail.ru', '$2y$10$TgtZnnYUUKHpIU5JotofFu42KJTEPmJ6OeRB15OkmQJpYaL4/M52O', '2020-07-22 11:55:12', '2020-08-03 19:00:12', '5f032d6d2b81c01.jpg', '1', null, 'admin', '1596446186', '2020/08/03');
INSERT INTO `users` VALUES ('22', 'Marduk', 'Azganun', '69', 'asasdasdasd@mail.com', '$2y$10$c4KroejZNedUOVjp.OVFXOXP5nWShL1NmWtKQyCLK.qv2BH/4E8hi', '2020-07-28 12:55:01', '2020-08-02 19:27:49', 'default.png', '1', null, 'user', '1596382069', '2020/08/01');
INSERT INTO `users` VALUES ('23', 'Admin', 'Admin', '43', 'aghsudhgkasj@mail.ru', '$2y$10$kczPVd8bG3zX5nohySMw.u732XP5Of/mZdWrqG4YNtQYFDBpcm8qK', '2020-07-31 16:26:22', '2020-07-31 16:26:22', 'default.png', '1', null, 'admin', '0', '0');
INSERT INTO `users` VALUES ('24', 'Mr', 'Paron', '12', 'sraphyeaxpkguddmon@ttirv.org', '$2y$10$2tmdlm1tSuu8tKb2DZl7wOWHOsj4H6cEvhyaM8neF4rdlZQfZXpVS', '2020-08-02 20:57:51', '2020-08-03 22:47:08', 'default.png', '1', null, 'user', '1596481028', '2020/08/02');
INSERT INTO `users` VALUES ('25', 'asdasd', 'gasdasd', '48', 'xuuatehcvsfkyqbrba@ttirv.org', '$2y$10$M1Gcphf85M3Cbrwewoc6P.BwiQ0BBAvDt3Dwl3ZD.EtM8ocdjG59K', '2020-08-04 01:02:20', '2020-08-04 01:25:48', 'default.png', '1', 'zIXnZP', 'user', '0', '2020/08/04');
INSERT INTO `users` VALUES ('26', 'asdasd', 'gasdasd', '19', 'ofibxdzphfgfbmwqbt@ttirv.org', '$2y$10$tuA4k0MAQ5umMKZ0GAw.JuzjcKd2WjL7ECFz/sDxtloOACJj7U3lu', '2020-08-04 01:17:40', '2020-08-04 01:25:31', 'default.png', '1', null, 'user', '0', '2020/08/04');
INSERT INTO `users` VALUES ('27', 'ASDasd', 'Nasdp', '43', 'ijwiflihnvmqubolyy@ttirv.net', '$2y$10$edMlVUDaEeYf4QADN52naerhdKJ3fE/FuhfxF/ZVT.cFKCZNnjmoi', '2020-08-04 09:33:10', '2020-08-04 09:43:19', '5f28f34593001p7.jpg', '1', null, 'user', '1596519712', '2020/08/04');
INSERT INTO `users` VALUES ('28', 'asdasf', 'asfadsd', '32', 'dgbmuitcvhozmbuesx@ttirv.com', '$2y$10$y1yvTBol0GW6YK7/D/S8i.uX/lEstytwzpjI.vWXk7B2Mw7AMeBj2', '2020-08-04 09:44:22', '2020-08-04 09:46:52', 'default.png', '1', null, 'user', '0', '2020/08/04');

-- ----------------------------
-- Table structure for whishlist
-- ----------------------------
DROP TABLE IF EXISTS `whishlist`;
CREATE TABLE `whishlist` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `whishlist_product_id_foreign` (`product_id`),
  KEY `whishlist_user_id_foreign` (`user_id`),
  CONSTRAINT `whishlist_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `whishlist_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of whishlist
-- ----------------------------
INSERT INTO `whishlist` VALUES ('22', '11', '1', '2020-08-03 17:01:35', '2020-08-03 17:01:35');
INSERT INTO `whishlist` VALUES ('25', '45', '3', '2020-08-04 01:27:14', '2020-08-04 01:27:14');
SET FOREIGN_KEY_CHECKS=1;
