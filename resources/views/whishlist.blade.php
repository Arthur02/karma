@extends('layouts.UserLayout')
@section('title','Karma Whishlist')
@section('content')
<!-- Start Header Area -->
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/homepage.css') }}">
</head>
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end" style="position: relative;">
			<div class="col-first aosdk" style="position: relative;">
				<h1>Shopping Cart</h1>
				<nav class="d-flex align-items-center">
					<a href="shop">Shop<span class="lnr lnr-arrow-right"></span></a>
					<a href="login">Cart</a>
				</nav>
			</div>
		</div>
	</div>
</section>

		{{-- @dd($products) --}}
		<div id="home" class="container tab-pane active"><br>
			<div class="w-100">
				<section style="padding-top: 20px; padding-bottom: 100px;">
		        <div class="container">
		            <div class="cart_inner">
		                <div class="table-responsive">
		                    <table class="table">
		                        <thead>
		                            <tr>
		                                <th scope="col">Product</th>
		                                <th scope="col">Price</th>
		                                <th scope="col">Move To Cart</th>
		                                <th scope="col">Delete</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            @foreach($products as $product)
		                            <tr style="position: relative;" class="incpetqaanpaymanjnjeqwhish">
										
		                                <td>
		                                    <div class="media">
		                                        <div class="d-flex">
		                                            <img width="130px" src="{{asset("img/product/".$product->productimages[0]->photo) }}" alt="">
		                                        </div>
		                                        <div class="media-body">
		                                            <p>{{$product->products->name}}</p>
		                                        </div>
		                                    </div>
		                                </td>
		                                <td>
		                                    <h5>$<span class="ginnn">{{ $product->products->price }}</span>.00</h5>
		                                </td>
		                                <td>
											<button class="primary-btn getouttocart" style="border:none" id="{{ $product->products->id }}">Move
											</button>
											
		                                </td>
		                                <td style="position: relative;">
											<button class="primary-btn dltfromwhish" style="color: #fff; background: #f44a40; border: none"  id="{{ $product->id }}">Delete</button>
											
		                                </td>
		                            </tr>
		                                @endforeach
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </section>
			</div>
		</div>

@endsection('content')
