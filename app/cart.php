<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    public $table = 'cart';
	public function products(){
   		return $this->belongsTo("App\products", "product_id");
	}
	public function productimages(){
   		return $this->hasmany("App\productphoto", "product_id","product_id");
	}
}
