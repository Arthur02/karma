@extends('layouts.UserLayout')
@section('title','Karma SingleProduct')


@section('content')
<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Product Details Page</h1>
					<nav class="d-flex align-items-center">
						<a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="#">Shop<span class="lnr lnr-arrow-right"></span></a>
						<a href="single-product.html">product-details</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	{{-- {{ dd($product->photos) }} --}}
	<input type="hidden" name="" id="{{ Session::get('id') }}" class="asdasdasd">
	<!--================Single Product Area =================-->
	<div class="product_image_area">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="s_Product_carousel">
						@if(count($product->photos) == 1)
							<div class="single-prd-item">
								<img class="img-fluid" src="{{asset("img/product/".$product->photos[0]['photo']) }}" alt="">
							</div>
							<div class="single-prd-item">	
								<img class="img-fluid" src="{{asset("img/product/".$product->photos[0]['photo']) }}" alt="">
							</div>
						@else
						@foreach($product->photos as $photo)
							{{-- {{ $a = $photo['photo'] }} --}}
							<div class="single-prd-item">	
								<img class="img-fluid" src="{{asset("img/product/".$photo['photo']) }}" alt="">
							</div>
						@endforeach
						@endif
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h3 class="prodddddname">{{ $product->name }}</h3>
						<h2 class="prodddddprice">${{ $product->price }}.00</h2>
						<ul class="list">
							<li><a class="active" href="#"><span>Category</span> : <span class="prodddddcategory">{{ $product->category->name }}</span></a></li>
							<li><a href="#"><span>Availibility</span> : <span class="prodddddcount">{{ $product->count }} left</span></a></li>
						</ul>
						<p class="proddddddesc">{{$product->description}}</p>
						@if($product->user_id != session::get('id'))
							<div class="product_count">
								<label for="qty">Quantity:</label>
								@if($cart)
								<input type="text" class="myvalisyours" name="qty" id="sst" value="{{ $cart->count }}" title="Quantity:" class="input-text qty" >
								@else
								<input type="text" class="myvalisyours" name="qty" id="sst" value="1" title="Quantity:" class="input-text qty" >
								@endif
								<button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
								 class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
								<button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
								 class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
							</div>
						<div class="card_area d-flex align-items-center">
							<a class="primary-btn addtokart text-light" id="{{ $product->id }}" data_id="{{ $product->count }}">Add to Cart</a>
							<a class="icon_btn srtikqic" style="color:white; cursor:pointer;" id="{{ $product->id }}"><i class="lnr lnr lnr-heart"></i></a>
						</div>
						@else
								<button class="primary-btn" style="border:none" id="{{ $product->id }}" data-toggle="modal" data-target="#openediting">Edit</button>
								<button class="primary-btn" style="border:none" id="{{ $product->id }}" data-toggle="modal" data-target="#openeditingphoto">Edit Photos</button>
								<button class="primary-btn dltprod" style="color: #fff; background: #f44a40; border: none"  id="{{ $product->id }}">Delete</button>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--================End Single Product Area =================-->

	<!--================Product Description Area =================-->
	<section class="product_description_area">
		<div class="container">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
					 aria-selected="false">Specification</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
					 aria-selected="false">Comments</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
					 aria-selected="false">Reviews</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
					<p>{{$product->description}}</p>
				</div>
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<h5>Width</h5>
									</td>
									<td>
										<h5>128mm</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Height</h5>
									</td>
									<td>
										<h5>508mm</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Depth</h5>
									</td>
									<td>
										<h5>85mm</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Weight</h5>
									</td>
									<td>
										<h5>52gm</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Quality checking</h5>
									</td>
									<td>
										<h5>yes</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Freshness Duration</h5>
									</td>
									<td>
										<h5>03days</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>When packeting</h5>
									</td>
									<td>
										<h5>Without touch of hand</h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5>Each Box contains</h5>
									</td>
									<td>
										<h5>60pcs</h5>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					<div class="row">
						<div class="col-lg-6">
							<div class="comment_list">
								@foreach($review as $gnahat)
								<div class="review_item">
									<div class="media">
										<div class="d-flex">
											<img class="rounded-circle" width="50px" height="50px" style="object-fit: cover;" src="{{ asset('/userphotos/'.$gnahat->userner->image) }}" alt="">
										</div>
										<div class="media-body">
											<h4>{{ $gnahat->userner->name }} {{ $gnahat->userner->surname }}</h4>
											<h5>{{ $gnahat->created_at }}</h5>
										</div>
									</div>
									<p>{{ $gnahat->comment }}</p>
								</div>
								@endforeach
							</div>
						</div>
						<div class="col-lg-6">
							<div class="review_box">
								<h4>Your Comment</h4>
								@foreach($review as $gnahat)
									@if($gnahat->user_id == Session::get('id'))
										<p>{{ $gnahat->comment }}</p>
									@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="review-tab">
					<div class="row">
						<div class="col-lg-6">
							<div class="row total_rate">
								<div class="col-6">
									<div class="box_total">
										<h5>Overall</h5>
										<h4>{{ $product->rating }}.0</h4>
										<h6>({{ $product->rewqan }} Reviews)</h6>
									</div>
								</div>
								<div class="col-6">
									<div class="rating_list">
										<h3>Based on {{ $product->rewqan }} Reviews</h3>
										<ul class="list">
											@foreach($astxer as $a)
											<li>
												<a href="#">
													{{ $a->rate }} Star 
													@for($i = 0; $i<5; $i++)
														@if($a->rate>$i)
															<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>
														@else
														<i class="fa fa-star" aria-hidden="true" style="font-size: 15px; color: gray"></i>
														@endif
													@endfor
												</a>
											</li>
											@endforeach

								
										</ul>
									</div>
								</div>
							</div>
							{{-- @dd($review) --}}
							<div class="review_list mt-3">
								@foreach($review as $gnahat)
								<div class="review_item" style="position: relative;">
									<div class="media">
										<div class="d-flex">
											<img class="rounded-circle" width="50px" height="50px" style="object-fit: cover;" src="{{ asset('/userphotos/'.$gnahat->userner->image) }}" alt="">
										</div>
										<div class="media-body">
											<h4>{{ $gnahat->userner->name}} {{ $gnahat->userner->surname }}</h4>
											<span>{{ $gnahat->created_at }}</span><br>
													@for($i = 0; $i<5; $i++)
														@if($gnahat->rate>$i)
															<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>
														@else
														<i class="fa fa-star" aria-hidden="true" style="font-size: 15px; color: gray"></i>
														@endif
													@endfor
										</div>
									</div>
									<p>{{ $gnahat->comment }}</p>
								</div>
								@endforeach

							</div>
						</div>
						<div class="col-lg-6">
							<div class="review_box">
								<h4>Your Review</h4>
								<p>Your Rating:</p>
								<ul class="list">
									@foreach($review as $rat)
										@if($rat->user_id == Session::get('id'))
													@for($i = 0; $i<5; $i++)
														@if($gnahat->rate>$i)
															<i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:15px"></i>
														@else
														<i class="fa fa-star" aria-hidden="true" style="font-size: 15px; color: gray"></i>
														@endif
													@endfor

										@endif
									@endforeach
								</ul>
								<p>Outstanding</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Product Description Area =================-->

	<!-- Start related-product Area -->
	<section class="related-product-area section_gap_bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 text-center">
					<div class="section-title">
						<h1>Last seled products</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9">
					<div class="row">
						@foreach($lastproducts as $l)
						<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
							<div class="single-related-product d-flex">
								<a href="{{ asset('shop/product/'.$l->getprods->id) }}"><img src="{{ asset("img/product/".$l->getprods->photos[0]->photo) }}" alt="" width="70px"></a>
								<div class="desc">
									<a href="{{ asset('shop/product/'.$l->getprods->id) }}" class="title">{{ $l->getprods->name }}</a>
									<div class="price">
										<h6>${{ $l->getprods->price }}.00</h6>
										<h6 class="l-through">${{ $l->getprods->price + 50 }}.00</h6>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				<div class="col-lg-3">
					<div class="ctg-right">
						<a href="#" target="_blank">
							<img class="img-fluid d-block mx-auto" src="{{ asset("img/category/c5.jpg") }}" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End related-product Area -->






		<!-- The Modal -->
		<div class="modal fade" id="openediting">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Edit Product</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>


					<!-- Modal body -->
					<div class="modal-body">
						<div class="productimages mx-auto" style="height: auto">
							@for($i = 0; $i<count($product->photos); $i++)
							{{-- @foreach($product->photos as $photo) --}}
								@if(isset($product->photos[$i]))
									<div class="d-inline-block h-50 mb-1" style="width: 32.5%; position:relative;">
										<img class="img-fluid" src="{{asset("img/product/".$product->photos[$i]['photo']) }}" alt="">
									</div>
								@endif
							@endfor
								{{-- <span class="upload"></span> --}}
							{{-- <span class="upload"></div> --}}
							{{-- @endforeach --}}
						</div>
						<div class="inputs mt-2">
								@if($errors->has('name'))
										<label for="name" class="text-danger mt-2">{{ $errors->first('name') }}</label>
									@else
										<label for="name" class="name1 mt-2 control-label">Name</label>
									@endif
								<input value="{{ $product->name }}" type="text" name="name" id="prname" placeholder="Product Name" value="{{ old('name') }}" class="form-control" autofocus>

	
								@if($errors->has('count'))
									<label for="lastName" class="text-danger mt-2">{{ $errors->first('count') }}</label>
								@else	
									<label for="lastName" class="count1 mt-2 control-label">Count</label>
								@endif
								<input value="{{ $product->count }}" type="text" name="count" id="count" placeholder="Product count" value="{{ old('count') }}" class="form-control" autofocus>

								
								@if($errors->has('price'))
									<label for="lastName" class="text-danger mt-2">{{ $errors->first('price') }}</label>
								@else
									<label for="price" class="price1 mt-2 control-label">Price</label>
								@endif
								<input value="{{ $product->price }}" type="text" name="price" id="price" class="form-control" placeholder="Product price" value="{{ old('price') }}">
								

								@if($errors->has('type'))
									<label for="lastName" class="text-danger mt-2">{{ $errors->first('type') }}</label>
								@else
									<label for="select" class="type1 w-100 mt-2 control-label">Type</label>
								@endif
									<select name="type" id="category" class="w-100 pasd" style="display: block !important;">
										<option disabled selected="" class="w-100">Product type</option>
										 @foreach($category as $cat)
                                            <option value="{{$cat->id}}" data-id="{{ $cat->name }}" class="w-100">{{$cat->name}}</option>
                                         @endforeach
									</select>
								<br>
							
								
								@if($errors->has('description'))
									<label for="lastName" class="text-danger mt-2">{{ $errors->first('description') }}</label>
								@else
									<label for="select" class="desc1 w-100 mt-2 control-label">Description</label>
								@endif
								<textarea class="single-textarea" name="description" id="description" placeholder="Product description" value="{{ old('description') }}">{{ $product->description }}</textarea>
						</div>
					</div>


					<!-- Modal footer -->
					<div class="modal-footer">
						<button class="genric-btn primary radius savechanges1 text-light" id="{{ $product->id }}" style="font-size: 14px;">Save</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>


{{-- modal 2 --}}
	<div class="modal fade" id="openeditingphoto">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Edit Product Photo</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<form class="login_form" action="{{ url('/editaddimage') }}" method="post" enctype="multipart/form-data">
						@csrf

						<!-- Modal body -->
						<div class="modal-body">
							
							<div class="productimages mx-auto" style="height: auto">
								<h3>Photos</h3>
								<hr>
								@for($i = 0; $i<count($product->photos); $i++)
								{{-- @foreach($product->photos as $photo) --}}
									@if(isset($product->photos[$i]))
										<div class="d-inline-block h-50 mb-1" style="width: 32.5%; position:relative;">
											<img class="img-fluid" src="{{asset("img/product/".$product->photos[$i]['photo']) }}" alt="">
											<p class="gnaankyun" title="Remove image" id="{{ $product->photos[$i]['id'] }}">&times;</p>
										</div>
									@endif
								@endfor
									{{-- <span class="upload"></span> --}}
								{{-- <span class="upload"></div> --}}
								{{-- @endforeach --}}
								<p class="text-danger">Warning! if you click 'x' the image will be removed</p>
						</div>
						<hr>
						<div class="new-photos mx-auto">
							<h3>New Photos</h3>
							<hr>
							
						</div>




							@if($errors->has('photo.*'))
									<label for="photo.*" class="text-danger mt-2">{{ $errors->first('photo.*') }}</label>
								@elseif($errors->has('photo'))
									<label for="photo" class="text-danger mt-2">{{ $errors->first('photo') }}</label>
								@else
									<label for="photo" class="photo1 mt-2 control-label">Images</label>
								@endif
								<br>
								<input type="file" name="photo[]" id="photo" accept=".jpg, .jpeg, .png" class="image_input" multiple>
						</div>


						<!-- Modal footer -->
						<div class="modal-footer">
							<button class="genric-btn primary radius savechanges text-light" id="{{ $product->id }}" name="id" value="{{ $product->id }}" style="font-size: 14px;">Save</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>
					</form>	

				</div>
			</div>
		</div>




@endsection('content')


{{-- 

            					@if($errors->has('photo.*'))
									<label for="photo.*" class="text-danger mt-2">{{ $errors->first('photo.*') }}</label>
								@elseif($errors->has('photo'))
									<label for="photo" class="text-danger mt-2">{{ $errors->first('photo') }}</label>
								@else
									<label for="photo" class="photo1 mt-2 control-label">Images</label>
								@endif
								<br>
								<input type="file" name="photo[]" id="photo" accept=".jpg, .jpeg, .png" class="image_input" multiple> --}}