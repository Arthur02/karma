<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('admin/getUsers', 'AdminController@getUsers');	
Route::get('admin/getProducts', 'AdminController@getProducts');	
Route::post('admin/blockUser', 'AdminController@blockUser');	
Route::post('admin/getAllProducts', 'AdminController@getAllProducts');	
Route::post('admin/unblockUser', 'AdminController@unblockUser');	
Route::post('admin/acceptproduct', 'AdminController@acceptproduct');	
Route::post('admin/denyproduct', 'AdminController@denyproduct');	
Route::post('admin/getname', 'AdminController@getname');	
Route::post('admin/getmessages', 'AdminController@getmessages');	
Route::post('admin/sendmessage', 'AdminController@sendmessage');	
