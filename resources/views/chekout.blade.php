@extends('layouts.UserLayout')
@section('title','Karma Checkout')
@section('content')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Checkout</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Shop<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Checkout</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
 @if(!empty($products[0]))
<section class="checkout_area section_gap">
        <div class="container">

            <div class="billing_details">
                <div class="row">
                    <div class="mx-auto w-100">
                        <div class="order_box">
                            <h2>Your Order</h2>
                            <ul class="list">
                                <li><a href="#">Product <span>Total</span></a></li>
                                @foreach($products as $product)
	                                <li><a href="{{ url('/shop/product/'.$product->products->id) }}">{{ $product->products->name }} <span class="middle">x {{ $product->count }}</span> <span class="last">${{ $product->products->price * $product->count }}.00</span></a></li>
                                @endforeach
                            </ul>
                            <ul class="list list_2">
                                <li><a href="#">Subtotal <span>${{ $total }}.00</span></a></li>
                                <li><a href="#">Shipping <span>Flat rate: $50.00</span></a></li>
                                <li><a href="#">Total <span>${{ $total + 50 }}.00</span></a></li>
                            </ul>
                            <div class="creat_account">
                                <input type="checkbox" id="f-option4" name="selector">
                                <label for="f-option4">I’ve read and accept the </label>
                                <a href="#">terms & conditions*</a>
                            </div>
                            <a class="primary-btn" href="{{url('/stripe')}}">Proceed to Paypal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
	<section class="checkout_area section_gap">
        <div class="container">

            <div class="billing_details">
                <div class="row">
                    <div class="mx-auto w-100">
                        <div class="order_box">
                            <h2 class="text-danger">Your cart is empty</h2>
                            <ul class="list">
                                <li><a href="#">Product <span>Total</span></a></li>
                                {{-- @foreach($products as $product) --}}
	                                {{-- <li><a href="{{ url('/shop/product/'.$product->products->id) }}">{{ $product->products->name }} <span class="middle">x {{ $product->count }}</span> <span class="last">${{ $product->products->price * $product->count }}.00</span></a></li> --}}
                                {{-- @endforeach --}}
                            </ul>
                            <ul class="list list_2">
                                <li><a href="#">Subtotal <span>$0.00</span></a></li>
                                <li><a href="#">Shipping <span>Flat rate: $0.00</span></a></li>
                                <li><a href="#">Total <span>$0.00</span></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif


@endsection('content')
