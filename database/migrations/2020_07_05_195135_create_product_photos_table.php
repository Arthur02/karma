<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ProductPhotos')) {
            Schema::create('ProductPhotos', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('photo');
                $table->unsignedBigInteger("product_id");
                $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('product_photos');
    }
}
