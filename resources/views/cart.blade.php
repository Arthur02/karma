@extends('layouts.UserLayout')
@section('title','Karma Cart')
@section('content')
<!-- Start Header Area -->
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/homepage.css') }}">
</head>
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end" style="position: relative;">
			<div class="col-first aosdk" style="position: relative;">
				<h1>Shopping Cart</h1>
				<nav class="d-flex align-items-center">
					<a href="shop">Shop<span class="lnr lnr-arrow-right"></span></a>
					<a href="login">Cart</a>
				</nav>
			</div>
		</div>
	</div>
</section>

		{{-- @dd($products) --}}
		<div id="home" class="container tab-pane active"><br>
			<div class="w-100">
				<section style="padding-top: 20px; padding-bottom: 100px;">
		        <div class="container">
		            <div class="cart_inner">
		                <div class="table-responsive">
		                    <table class="table">
		                        <thead>
		                            <tr>
		                                <th scope="col">Product</th>
		                                <th scope="col">Price</th>
		                                <th scope="col">Quantity</th>
		                                <th scope="col">Total</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            @foreach($products as $product)
		                            <tr style="position: relative;" class="incpetqaanpaymanjnjeq">
										
		                                <td>
		                                    <div class="media">
		                                        <div class="d-flex">
		                                            <img width="130px" src="{{asset("img/product/".$product->productimages[0]->photo) }}" alt="">
		                                        </div>
		                                        <div class="media-body">
		                                            <p>{{$product->products->name}}</p>
		                                        </div>
		                                    </div>
		                                </td>
		                                <td>
		                                    <h5>$<span class="ginnn">{{ $product->products->price }}</span>.00</h5>
		                                </td>
		                                <td>
		                                    <div class="product_count">
		                                        <input type="text" name="qty" id="sst" maxlength="12" value="{{ $product->count }}" title="Quantity:"
		                                            class="input-text qty">
		                                        <button  class="increase items-count getupman" id={{ $product->id }}>
		                                        	<i class="lnr lnr-chevron-up"></i>
		                                        </button>
		                                        <button class="reduced items-count getdownman" id="{{ $product->id }}">
		                                        	<i class="lnr lnr-chevron-down"></i>
		                                        </button>
		                                    </div>
		                                </td>
		                                <td style="position: relative;">
		                                	<p style="position: absolute; right: 10px; top: 10px; cursor: pointer;" class="removefromcart" id="{{ $product->products->id }}">&times;</p>

		                                    <h5 class="obshiginy">$720.00</h5>
		                                </td>
		                            </tr>
		                                @endforeach
		                            <tr class="bottom_button">
		                                <td>
		                                    <a class="gray_btn" href="{{ url('/allProduct') }}">Continue shop</a>
		                                </td>
		                                <td>

		                                </td>
		                                <td>

		                                </td>
		                                <td style="position: relative;">
		                                    <div class="cupon_text d-flex align-items-center" style="position: absolute;right: 10px; top: 0;bottom: 0;">
		                                        <a class="primary-btn" href="{{  url('/checkme')  }}">Process to checkout</a>
		                                    </div>
		                                </td>
		                            </tr>
		                            <tr>
		                                <td>

		                                </td>
		                                <td>

		                                </td>
		                                <td>
		                                    <h5>Subtotal</h5>
		                                </td>
		                                <td>
		                                    <h5 class="subsax">$2160.00</h5>
		                                </td>
		                            </tr>
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </section>
			</div>
		</div>

@endsection('content')
