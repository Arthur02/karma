<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="{{asset ('css/linearicons.css')}}">
	<link rel="stylesheet" href="{{asset ('css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset ('css/themify-icons.css')}}">
	<link rel="stylesheet" href="{{asset ('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset ('css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset ('css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset ('css/nouislider.min.css')}}">
	<link rel="stylesheet" href="{{asset ('css/ion.rangeSlider.css')}}">
	<link rel="stylesheet" href="{{asset ('css/ion.rangeSlider.skinFlat.css')}}">
	<link rel="stylesheet" href="{{asset ('css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset ('css/main.css')}}">
	<link rel="stylesheet" href="{{asset ('css/message.css')}}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

	<input type="hidden" name="" value="{{ Session::get('id') }}" class="userid">
	<div id="app">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<ul class="navbar-nav container">
				<li class="nav-item">
					<router-link class="nav-link" to="/admin/users">Users</router-link>
				</li>
				<li class="nav-item">
					<router-link class="nav-link" to="/admin/products">New Products</router-link>
				</li>
				<li class="nav-item">
					<router-link class="nav-link" to="/admin/allproducts">All Products</router-link>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ url('/logOut') }}">Log Out</a>
				</li>
			</ul>
		</nav>

		<router-view></router-view>

		
	</div>

</body>
	<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</html>