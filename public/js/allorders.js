jQuery(document).ready(function($) {
  	let token = $('meta[name="csrf-token"]').attr('content')
	$('.allordersimihat').click(function(event) {
		let id = $(this).attr('id');
		$.ajax({
			type:'post',
			url:'/getthisonesinfo',
			data:{'_token':token,'id':id},
			success:function(r){
				$('.ekeqstexavelaceq').empty()
				r = JSON.parse(r)
				console.log(r)
				$(`<section class="checkout_area mb-5">
			        <div class="container">

			            <div class="billing_details">
			                <div class="row">
			                    <div class="mx-auto w-100">
			                        <div class="order_box">
			                            <h2>Your Order</h2>
			                            <ul class="list imaracpraductneryarac">
			                                <li><a href="#">Product <span>Total</span></a></li>

			                            </ul>
			                            <ul class="list list_2">
			                                <li><a href="#">Subtotal <span>$${r.total}.00</span></a></li>
			                                <li><a href="#">Shipping <span>Flat rate: $50.00</span></a></li>
			                                <li><a href="#">Total <span>$${r.total + 50}.00</span></a></li>
			                            </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </section>`).appendTo('.ekeqstexavelaceq ')
			    r.ordprod.forEach(function(e) {
					$(`
							<li style="position:relative" class="gtirincstexem">
								<a href="/shop/product/${e.id}">
								${ e.name } <span class="middle">x <span class="esqanaky"></span></span>
								<span class="last">$${ e.price }.00</span>
								</a>
								<div class="escezgnahatumem">
									<span class="qliiiiq" style="font-size:20px" data-id="1" id="${e.id}">☆</span>
									<span class="qliiiiq" style="font-size:20px" data-id="2" id="${e.id}">☆</span>
									<span class="qliiiiq" style="font-size:20px" data-id="3" id="${e.id}">☆</span>
									<span class="qliiiiq" style="font-size:20px" data-id="4" id="${e.id}">☆</span>
									<span class="qliiiiq" style="font-size:20px" data-id="5" id="${e.id}">☆</span>
								</div>
								<textarea class="imtexttostar stexgrelen" placeholder="Your Message here" style="display:none; width:90%"></textarea>
								<button class="primary-btn btn imtexttostar incpetqaqliqel" style="display:none; vertical-align:top" id="${e.id}">Save</button>
							</li>
					`).appendTo('.imaracpraductneryarac')
			    r.detals.forEach(function(f){
   					if(f.product_id == e.id){
              			$(".imaracpraductneryarac li").last().find(".esqanaky").text(f.count)
    			 	}
				})
			    });
			}
		})
	});
	$('.prevent').click(function(event) {
		event.preventDefault()
	});

	let gnahatakan
	$(document).on('click', '.qliiiiq', function(event) {
		$('.imtexttostar').css({
			display:'none',
		})
		$(this).parent().find('.qliiiiq').css({
		   	content: '\2605',
   			color: 'black',
   			unicodeBidi: 'bidi-override',
   			direction: 'ltr',
		})
		let id = $(this).attr('id');
		gnahatakan = $(this).attr('data-id')
		for(let i = gnahatakan-1 ; i<5; i++){
			$(this).parent().find('.qliiiiq').eq(i).css({
				   	content: '\2605',
   					color: 'gold',
    				unicodeBidi: 'bidi-override',
    				direction: 'ltr',
			})
		}
		$(this).parents('.gtirincstexem').find('.imtexttostar').css({
			display:'inline-block',
		})
	});
	$(document).on('click', '.incpetqaqliqel', function(event) {
		if(gnahatakan>3){
			gnahatakan = 5 - 4 + (5 - gnahatakan) 
		}
		else{
			gnahatakan = 1 + 4 + (1 - gnahatakan)
		}
		let id = $(this).attr('id')
		let message = $(this).parent().find('.stexgrelen').val()
		$.ajax({
			type:'post',
			url:'/ratingsystem',
			data:{'_token':token,'id':id,'message':message,'gnahatakan':gnahatakan},
			success:function(r){
				console.log(r)
				Swal.fire(
				  'Good job!',
				  'Your rate is added you can change it anytime!',
				  'success'
				)
			}
		})
	});

});