<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	{{-- <link rel="shortcut icon" href="img/fav.png"> --}}
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')</title>
	<!--
		CSS
		============================================= -->
	<link rel="stylesheet" href="{{asset ('css/linearicons.css')}}">
	<link rel="stylesheet" href="{{asset ('css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset ('css/themify-icons.css')}}">
	<link rel="stylesheet" href="{{asset ('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset ('css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset ('css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset ('css/nouislider.min.css')}}">
	<link rel="stylesheet" href="{{asset ('css/ion.rangeSlider.css')}}">
	<link rel="stylesheet" href="{{asset ('css/ion.rangeSlider.skinFlat.css')}}">
	<link rel="stylesheet" href="{{asset ('css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset ('css/main.css')}}">
	<link rel="stylesheet" href="{{asset ('css/message.css')}}">
	<link rel="shortcut icon" href="">
</head>
<body>
	@csrf

	<!-- Start Header Area -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="/allProduct"><img src="{{ asset('img/logo.png') }}" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item submenu dropdown 
								 @if(request()->is('allProduct') || request()->is('myprod') || request()->is('/') || request()->is('addProduct') || request()->is('cart') || request()->is('whishlist')|| request()->is('checkme')|| request()->is('conform')   )
									 active
								 @endif
								 ">
								<a href="/allProduct" class="nav-link dropdown-toggle" role="button" aria-haspopup="true"
								 aria-expanded="false">Shop</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="{{ url('/addProduct') }}">Add product</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ url('/whishlist') }}">My Whishlist</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ url('/cart') }}">Shopping Cart</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ url('/checkme') }}">Product Checkout</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ url('/conform') }}">Orders</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ url('/myprod') }}">My Products</a></li>
								</ul>
							</li>
							<li class="nav-item {{ request()->is('home') ? 'active' : null }}"><a class="nav-link" href="/home">Me</a></li>
	{{-- 						<li class="nav-item submenu dropdown ">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Blog</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="/blog.html">Blog</a></li>
									<li class="nav-item"><a class="nav-link" href="/single-blog.html">Blog Details</a></li>
								</ul>
							</li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Pages</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="/tracking.html">Tracking</a></li>
									<li class="nav-item"><a class="nav-link" href="/elements.html">Elements</a></li>
								</ul>
							</li> --}}
							<li class="nav-item"><a class="nav-link" href="{{ asset('contact') }}">Contact</a> <span style="position: relative; top: -10px;" class="messagecount"></span></li>

						</ul>
						@if(isset($user['id']))
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item"><a class="nav-link" href="/logOut">log out</a></li>
						</ul>
						@else
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item"><a class="nav-link" href="/login">Log in</a></li>
						</ul>
						@endif
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="{{ url('notifications') }}" class="cart"><span class=""><i class="fa fa-bell"></i></span></a></li>
								<span style="position: relative; top: 10px;" class="notificationcount"></span>
							<li class="nav-item"><a href="{{ url('/cart') }}" class="cart"><span class="ti-bag"></span></a></li>
							<li class="nav-item">
								<button class="search"><span class="lnr lnr-magnifier" id="search"></span></button>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="search_input" id="search_input_box">
			<div class="container">
				<form class="d-flex justify-content-between" action="{{ url('search_result') }}" method="post">
					@csrf
					<input type="text" class="form-control" id="search_input" placeholder="Search Here" name="key">
					<button class="btn mr-4 mt-2 text-light" id="searrch" title="Search"><span class="lnr lnr-magnifier"></span></button>
					<button type="submit" class="btn"></button>
					<span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
				</form>
			</div>
		</div>
	</header>




	@yield('content')


		<!-- start footer Area -->
	<footer class="footer-area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>About Us</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore
							magna aliqua.
						</p>
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Newsletter</h6>
						<p>Stay update with our latest</p>
						<div class="" id="mc_embed_signup">

							<form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
							 method="get" class="form-inline">

								<div class="d-flex flex-row">

									<input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '"
									 required="" type="email">


									<button class="click-btn btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
									<div style="position: absolute; left: -5000px;">
										<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
									</div>

									<!-- <div class="col-lg-4 col-md-4">
												<button class="bb-btn btn"><span class="lnr lnr-arrow-right"></span></button>
											</div>  -->
								</div>
								<div class="info"></div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-3  col-md-6 col-sm-6">
					<div class="single-footer-widget mail-chimp">
						<h6 class="mb-20">Instragram Feed</h6>
						<ul class="instafeed d-flex flex-wrap">
							<li><img src="{{ asset('img/i1.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i2.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i3.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i4.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i5.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i6.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i7.jpg') }}" alt=""></li>
							<li><img src="{{ asset('img/i8.jpg') }}" alt=""></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6>Follow Us</h6>
						<p>Let us be social</p>
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				<p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->

	<script src="{{URL::asset('js/vendor/jquery-2.2.4.min.js')}}"></script>
	<script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js')}}" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	 crossorigin="anonymous"></script>
	<script src="{{URL::asset('js/vendor/bootstrap.min.js')}}"></script>
	<script src="{{URL::asset('js/jquery.ajaxchimp.min.js')}}"></script>
	<script src="{{URL::asset('js/jquery.nice-select.min.js')}}"></script>
	<script src="{{URL::asset('js/jquery.sticky.js')}}"></script>
	<script src="{{URL::asset('js/nouislider.min.js')}}"></script>
	{{-- <script src="{{URL::asset('js/countdown.js')}}"></script> --}}
	<script src="{{URL::asset('js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
	<!--gmaps Js-->
	<script src="{{URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE')}}"></script>
	<script src="{{URL::asset('js/gmaps.min.js')}}"></script>
	<script src="{{URL::asset('js/main.js')}}"></script>
	<script src="{{ URL::asset('https://cdn.jsdelivr.net/npm/sweetalert2@9') }}"></script>
	<script src="{{ URL::asset('js/singleproduct.js') }}"></script>
	<script src="{{ URL::asset('js/myprod.js') }}"></script>
	<script src="{{ URL::asset('js/shop.js') }}"></script>
	<script src="{{ URL::asset('js/cart.js') }}"></script>
	<script src="{{ URL::asset('js/whishlist.js') }}"></script>
	<script src="{{ URL::asset('js/allorders.js') }}"></script>
</body>

</html>