<?php

namespace App\Http\Middleware;

use Closure;
use App\Users;
use Session;
use Redirect;

class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session::has('id')){
            $user = Users::where('id',session::get('id'))->first();
            if($user){
                if($user->type != 'admin'){
                    return Redirect::to('/');
                }
                else{
                    return $next($request);
                }
            }
            else{
                return Redirect::to('/');
            }
        }
        else{
            return Redirect::to('/');
        }
    }
}
