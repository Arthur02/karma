<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LARAVEL Example</title>
</head>
<body>
	{{-- {{ dd($users) }} --}}
	<h1 style="color: #3ae; font-family: gabriola">Hello Laravel </h1>
	<p>This is {{ $name }}'s first Laravel project</p>
	<ul>
		@foreach($array as $item)
			<li>list {{ $item }}</li>
		@endforeach
	</ul>
	<div>
		@foreach($users as $user)
			<div style="border:1px solid #000; width: 200px; height: 200px; background: #a3e; display: inline-block; vertical-align: top">
				<h2> {{ $user['name'] }} </h2>
				<h2> {{ $user['surname'] }} </h2>
				
	{{-- 			@if($user['age'] > 18)

					<h2 style="background:lime"> {{ $user['age'] }} </h2>
				@else

					<h2 style="background:red"> {{ $user['age'] }} </h2>

				@endif --}}
				
					<h2 style="background:{{ ($user['age'] > 18)?'lime':'red' }}"> {{ $user['age'] }} </h2>
			</div>
		@endforeach
	</div>
</body>
</html>