@extends('layouts.UserLayout')
@section('title','Karma Change Password')
@section('content')
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Login/Register</h1>
					<nav class="d-flex align-items-center">
						<a href="shop">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="login">Login/Register</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<div class="col-lg-6 mx-auto mb-5 mt-5">
		<div class="login_form_inner">
			<h3>Forgot password</h3>
			<form class="row login_form" action="{{ url('/changemypassword') }}" method="post" id="contactForm">
				@csrf
				@if(Session::has('saaaaaxnormala'))
				<label class="mx-auto text-success">We sent code to your email address</label>
				@endif
				<div class="col-md-12 form-group">
					<label class="text-danger">{{ $errors->first('code') }}</label>
					<input type="text" class="form-control" id="code" name="code" placeholder="Code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Code'" value="{{ old('code') }}">
				</div>
				<div class="col-md-12 form-group">
					<label class="text-danger">{{ $errors->first('password') }}</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="New Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'New Password'">
				</div>
				<div class="col-md-12 form-group">
					<label class="text-danger">{{ $errors->first('confirmpassword') }}</label>
					<input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirm Password'">
				</div>
				<div class="col-md-12 form-group">
					<button type="submit" value="submit" class="primary-btn">Save</button>
				</div>
			</form>
		</div>
	</div>
@endsection