<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Builder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
    }
    function boot()
	{
	    Builder::defaultStringLength(191); // Update defaultStringLength
	}
}
