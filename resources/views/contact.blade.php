@extends('layouts.UserLayout')
@section('title','Karma Add Contact')
@section('content')
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Contact with Admins</h1>
					<nav class="d-flex align-items-center">
						<a href="home">Me<span class="lnr lnr-arrow-right"></span></a>
						<a href="login">Contact</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<div class="container mt-5 mb-5">
		@foreach($admins as $admin)
			<div class="form-control" style="height: auto; position: relative;">
				<img src="{{ asset('userphotos/'.$admin->image) }}" class="rounded-circle" width="70px" height="70px" style="object-fit: cover;">
				<h3 class="d-inline-block">{{ $admin->name }} {{ $admin->surname }}</h3>
				<button class="btn primary-btn w-50 openmes" style="position: absolute; right: 0px;top: 0; bottom: 0;" id="{{ $admin->id }}"><i class="fa fa-envelope" aria-hidden="true"></i></button>
			</div>
		@endforeach
	</div>



	<div class="messagebox">
  <div class="chatername">
    <p class="closeme">&times;</p>
  </div>
  <hr>
  <div class="mejinna">
    
  </div>
  <hr>
  <div class="stexgreq" style="position: relative;">
    <input type="text" name="" class="form-control grelu" style="border:none; width:87%;" placeholder="Your message here">
    <button class="btn btn-success senda" style="position: absolute; right: 0; top: 0;"><i class="fa fa-paper-plane"></i></button>

  </div>
  
</div>
@endsection
