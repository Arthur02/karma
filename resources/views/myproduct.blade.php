@extends('layouts.UserLayout')
@section('title','Karma MyProduct')


@section('content')

<section class="banner-area organic-breadcrumb">
	<div class="container">
		<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
			<div class="col-first">
				<h1>My Products</h1>
				<nav class="d-flex align-items-center">
					<a href="shop">Shop<span class="lnr lnr-arrow-right"></span></a>
					<a href="login">My Products</a>
				</nav>
			</div>
		</div>
	</div>
</section>

		<div class="container w-90 mt-5">
			<div class="filter-bar d-flex flex-wrap align-items-center">
{{-- 					<div class="sorting">
						<select>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
						</select>
					</div>
					<div class="sorting mr-auto">
						<select>
							<option value="1" class="show12">Show 12</option>
							<option value="1" class="showall">Show All</option>
						</select>
					</div> --}}
					{{-- <div class="pagination"> --}}
						{{ $products->links() }}
					{{-- </div> --}}
				</div>
		</div>

{{-- {{ dd($products) }} --}}
<div class="container w-90">
	<section class="lattest-product-area pb-40 category-list emptyme">
		<!-- single product -->
			<div class="row">
		@foreach($products as $product)
				<div class="col-lg-4 col-md-6">
						<div class="single-product">
							<img class="img-fluid" src="{{asset("img/product/".$product->photos[0]['photo']) }}" alt="">
							<div class="product-details">
								<h6>{{$product->name}}</h6>
								<div class="price">
									<h6>${{$product->price}}.00</h6>
									<h6 class="l-through">${{ $product->price +50 }}.00</h6>
									@if($product->active == 1)
									<h6 class="text-success">Accepted</h6>
									@elseif($product->active == 2)
									<h6 class="text-danger">Denyed</h6>
									@elseif($product->active == 0)
									<h6 class="text-info">Not solowed</h6>
									@endif
								</div>
								<div class="prd-bottom">
									<a class="social-info clickforcompaere" style="cursor:pointer" id="{{$product->id}}">
										<span class="lnr lnr-sync"></span>
										<p class="hover-text">compare</p>
									</a>
									<a href="shop/product/{{ $product->id }}" class="social-info">
										<span class="lnr lnr-move"></span>
										<p class="hover-text">view more</p>
									</a>
								</div>
							</div>
						</div>
					</div>
			@endforeach
			</div>
	</section>

</div>



		<div class="container w-90 mb-5">
			<div class="filter-bar d-flex flex-wrap align-items-center">
{{-- 					<div class="sorting">
						<select>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
						</select>
					</div>
					<div class="sorting mr-auto">
						<select>
							<option value="1" class="show12" id="show">Show 12</option>
							<option value="1" class="showall" id="show">Show All</option>
						</select>
					</div> --}}
					{{-- <div class="pagination"> --}}
						{{ $products->links() }}
					{{-- </div> --}}
				</div>
		</div>

@endsection('content')




