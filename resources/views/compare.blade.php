@extends('layouts.UserLayout')
@section('title','Karma Compare')
@section('content')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Compare</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Shop<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Compare</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <div class="product_image_area">
        <div class="container" style="position: relative;">
            <div class="row s_product_inner  w-50">
                <div class="col-lg-6">
                    <div class="s_Product_carousel">
                        @if(count($prod1->photos) == 1)
                            <div class="single-prd-item">
                                <img class="img-fluid" src="{{asset("img/product/".$prod1->photos[0]['photo']) }}" alt="">
                            </div>
                            <div class="single-prd-item">   
                                <img class="img-fluid" src="{{asset("img/product/".$prod1->photos[0]['photo']) }}" alt="">
                            </div>
                        @else
                        @foreach($prod1->photos as $photo)
                            {{-- {{ $a = $photo['photo'] }} --}}
                            <div class="single-prd-item">   
                                <img class="img-fluid" src="{{asset("img/product/".$photo['photo']) }}" alt="">
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3 class="prodddddname">{{ $prod1->name }}</h3>
                        <h2 class="prodddddprice">${{ $prod1->price }}.00</h2>
                        <ul class="list">
                            <li><a class="active" href="#"><span>Category</span> : <span class="prodddddcategory">{{ $prod1->category->name }}</span></a></li>
                            <li><a href="#"><span>Availibility</span> : <span class="prodddddcount">{{ $prod1->count }} left</span></a></li>
                        </ul>
                        <p class="proddddddesc">{{$prod1->description}}</p>
                        <div class="card_area d-flex align-items-center">
                        </div>
                    </div>
                </div>
            </div>
            <div class="s_product_inner w-50 row" style="position: absolute; right: 0; top: 0;">
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3 class="prodddddname">{{ $prod2->name }}</h3>
                        <h2 class="prodddddprice">${{ $prod2->price }}.00</h2>
                        <ul class="list">
                            <li><a class="active" href="#"><span>Category</span> : <span class="prodddddcategory">{{ $prod2->category->name }}</span></a></li>
                            <li><a href="#"><span>Availibility</span> : <span class="prodddddcount">{{ $prod2->count }} left</span></a></li>
                        </ul>
                        <p class="proddddddesc">{{$prod2->description}}</p>
                        <div class="card_area d-flex align-items-center">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="s_Product_carousel">
                        @if(count($prod2->photos) == 1)
                            <div class="single-prd-item">
                                <img class="img-fluid" src="{{asset("img/product/".$prod2->photos[0]['photo']) }}" alt="">
                            </div>
                            <div class="single-prd-item">   
                                <img class="img-fluid" src="{{asset("img/product/".$prod2->photos[0]['photo']) }}" alt="">
                            </div>
                        @else
                        @foreach($prod2->photos as $photo)
                            {{-- {{ $a = $photo['photo'] }} --}}
                            <div class="single-prd-item">   
                                <img class="img-fluid" src="{{asset("img/product/".$photo['photo']) }}" alt="">
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
