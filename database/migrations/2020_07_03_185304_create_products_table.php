<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',20);
                $table->integer('price');
                $table->integer('count');
                $table->string('description');
                $table->unsignedBigInteger("user_id");
                $table->unsignedBigInteger("category_id");
                $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
                $table->foreign('category_id')->references('id')->on('category')
                ->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('products');
    }
}
