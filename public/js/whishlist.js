jQuery(document).ready(function($) {
  let token = $('meta[name="csrf-token"]').attr('content')
	$('.dltfromwhish').click(function(event) {
		let id = $(this).attr('id')
		$.ajax({
			type:'post',
			url:'/deletemywhish',
			data:{"_token":token,'id':id},
			success:function(r){
				Swal.fire(
				  'Sorry!',
				  'Your whish is gone!',
				  'success'
				)
			}
		})
		$(this).parents('.incpetqaanpaymanjnjeqwhish').remove()
	});
	$('.getouttocart').click(function(event) {
		let id = $(this).attr('id')
		$(this).parents('.incpetqaanpaymanjnjeqwhish').remove();
		$.ajax({
			type:'post',
			url:'/gnacartmti',
			data:{'_token':token,'id':id},
			success:function(r){
				Swal.fire(
				  'Yay!',
				  'This product added to your cart!',
				  'success'
				)
			}
		})
	});
	$('.stexpoxumemanuns').click(function(event) {
		let name = $('.qoanuny').val()
		let surname = $('.qoazganuny').val()
		$.ajax({
			type:'post',
			url:'/imchangingname',
			data:{'_token':token, 'name':name,'surname':surname},
			success:function(r){
				$('.tellyourname').html(name + ' ' + surname)
			}
		})
	});

	$('.closeme').click(function(){
		$('.messagebox').animate({
			bottom:'-500px'
		})
	})
	$('.openmes').click(function(event) {
		$('.mejinna').empty()
		let id = $(this).attr('id') 
		$('.messagebox').animate({
			bottom:'0'
		})
		$.ajax({
			type:'post',
			url:'getchaternameeeeee',
			data:{'_token':token,'id':id},
			success:function(r){
				r = JSON.parse(r)
				$('.usernamesname').remove()
				$('.chatername').append(`<h4 class="usernamesname d-inline-block">${r.name} ${r.surname}</h4>`)
				$('.senda').attr('id', `${r.id}`)
			}
		})
		$.ajax({
			type:'post',
			url:'getmymessageswithhim',
			data:{'_token':token,'id':id},
			success:function(r){
				r = JSON.parse(r)
				r.forEach(function(e){
						$(`

							<div class="uxakimes" id="${e.user_id}">
								<p>${e.message}</p>
								<p style="font-size:10px">${e.created_at}</p>
							</div>

							`).appendTo('.mejinna')
					})
					$('.uxakimes').each(function(el) {
		
						if($(this).attr('id') == id){
							$(this).addClass('mymessage')
						}
						else{
							$(this).addClass('hismessage')
						}
						
					});
			}
		})
	});
	$('.senda').click(function(event){
		let id = $(this).attr('id')
		let message = $('.grelu').val()
		if(message != ''){
			$.ajax({
				type:'post',
				url:'sendingmessage',
				data:{'_token':token,'id':id,'message':message},
				success:function(r){
					$(`

					<div class="mymessage uxakimes">
						<p>${message}</p>
					</div>


					`).appendTo('.mejinna')
					$('.grelu').val('')
				}
			})
		}
	})

});