require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import Axios from 'axios'
import $ from 'jquery'

import User from "./component/UserComponent.vue"
import Product from "./component/ProductComponent.vue"
import AllProduct from "./component/AllProductComponent.vue"
Vue.use(VueRouter)
Vue.use(VueAxios,Axios)

let r = new VueRouter({
    routes:[
        {path:"/admin/users", component: User},
        {path:"/admin/products", component: Product},
        {path:"/admin/allproducts", component: AllProduct},
    ],
    mode:'history'
})

let v = new Vue({
    router: r,
    el: "#app"
})