@extends('layouts.UserLayout')
@section('title','Karma Forgot')
@section('content')
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Login/Register</h1>
					<nav class="d-flex align-items-center">
						<a href="shop">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="login">Login/Register</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<div class="col-lg-6 mx-auto mb-5 mt-5">
		<div class="login_form_inner">
			<h3>Forgot password</h3>
			<form class="row login_form" action="{{ url('/imforgot') }}" method="post" id="contactForm">
				@csrf
				<div class="col-md-12 form-group">
					<label class="text-danger">{{ $errors->first('mail') }}</label>
					<input type="email" class="form-control" id="mail" name="mail" placeholder="Enter email of account" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email of account'" value="{{ old('mail') }}">
				</div>
				<div class="col-md-12 form-group">
					<button type="submit" value="submit" class="primary-btn">Search</button>
				</div>
			</form>
		</div>
	</div>


@endsection