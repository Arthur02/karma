@extends('layouts.UserLayout')
@section('title','Karma Orders')
@section('content')

<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>All Orders</h1>
					<nav class="d-flex align-items-center">
						<a href="index.html">Shop<span class="lnr lnr-arrow-right"></span></a>
						<a href="category.html">Orders</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Order Details Area =================-->
	<section class="order_details section_gap">
		<div class="container">
			@if(session::has('normaluxarkvav'))
			<h3 class="title_confirmation">Thank you. Your order has been received.</h3>
			<div class="row order_d_inner">
				<div class="col-lg-4">
					<div class="details_item">
						<h4>Order Info</h4>
						<ul class="list">
							<li><a href="#"><span>Order number</span> : {{ $orders[0]->id }}</a></li>
							<li><a href="#"><span>Date</span> : Los Angeles</a></li>
							<li><a href="#"><span>Total</span> : USD {{ $orders[0]->total + 50 }}</a></li>
							<li><a href="#"><span>Payment method</span> : Check payments</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="details_item">
						<h4>Billing Address</h4>
						<ul class="list">
							<li><a href="#"><span>Street</span> : 56/8</a></li>
							<li><a href="#"><span>City</span> : Los Angeles</a></li>
							<li><a href="#"><span>Country</span> : United States</a></li>
							<li><a href="#"><span>Postcode </span> : 36952</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="details_item">
						<h4>Shipping Address</h4>
						<ul class="list">
							<li><a href="#"><span>Street</span> : 56/8</a></li>
							<li><a href="#"><span>City</span> : Los Angeles</a></li>
							<li><a href="#"><span>Country</span> : United States</a></li>
							<li><a href="#"><span>Postcode </span> : 36952</a></li>
						</ul>
					</div>
				</div>
			</div>
			@endif
			{{-- @dd($orders[0]->ordprod[0]->name) --}}
<section class="checkout_area section_gap">
        <div class="container">

            <div class="billing_details">
                <div class="row">
                    <div class="mx-auto w-100">
                        <div class="order_box">
                            <h2>Your Orders</h2>
                            <ul class="list">
                                <li><a href="#">All your orders </a></li>

                            </ul>
                            <ul class="list list_2">
                            	@foreach($orders as $order)
                                {{-- <li><a href="#">Subtotal <span>${{ $total }}.00</span></a></li> --}}
                                	<li title="Open order" id="{{ $order->id }}" class="allordersimihat"><a href="#" class="prevent">{{ $order->ordprod[0]->name }} <span class="text-dark">Total: ${{ $order->total + 50 }}.00</span></a></li>
                                {{-- <li><a href="#">Total <span>${{ $total + 50 }}.00</span></a></li> --}}
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
		</div>
	<span class="ekeqstexavelaceq"></span>
	</section>
	<!--================End Order Details Area =================-->





@endsection('content')
