<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'category';
    public function photos(){
   		return $this->hasMany("App\ProductPhoto", "product_id");
	}
}
