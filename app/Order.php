<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'order';
    public function detals(){
   		return $this->hasMany("App\Orderdetales", "order_id");
	}
	public function ordprod(){
   		return $this->belongsToMany('App\Products','orderdetales','order_id','product_id');
	}
}
