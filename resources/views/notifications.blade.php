@extends('layouts.UserLayout')
@section('title','Karma Add Notifications')
@section('content')

	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Notifications</h1>
					<nav class="d-flex align-items-center">
						<a href="home">Me<span class="lnr lnr-arrow-right"></span></a>
						<a href="notifications">Notifications</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<div class="mt-5 mb-5 container">
	@if(count($notifications) == 0)
		<h1 class="text-danger">You Have Not Notifications</h1>
	@endif
		@foreach($notifications as $not)
		<a href="{{ asset('myprod') }}">
			<div class="form-control">
				<a href="{{ asset('/shop/product/'.$not->product_id) }}" class="d-inline-block">
					<h5>Your product {{ $not->name }} is <span class="@if($not->status == 'denyed')text-danger @else text-success @endif">{{ $not->status }}</span> </h5>
				</a>
			</div>
		</a>
		@endforeach
	</div>

@endsection