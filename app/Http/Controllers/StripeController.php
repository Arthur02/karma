<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cart;
use App\Order;
use App\Orderdetales;
use App\Users;
use App\Products;
use Session;
use Stripe;
use Redirect;

class StripeController extends Controller
{
    public function Stripe(){
        $user = Users::where('id',Session::get('id'))->first();
    	$products = cart::where('user_id',Session::get('id'))->with(['products'])->get();
        $total = 0;
        foreach($products as $product) {
            $total += $product->count * $product->products->price;
        }
        if($total > 0){
        	return view('stripe',compact('total','user'));
        }
        else{
        	return back();
        }



        // cart::where('user_id', session::get('id'))->delete();
        // $order = Order::where('user_id',session::get('id'))->get();
    }





    // post

    public function StripePost(Request $r){
    	$products = cart::where('user_id',Session::get('id'))->with(['products'])->get();
        $total = 0;
        foreach($products as $product) {
            $total += $product->count * $product->products->price;
        }
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
            "amount" => $total * 100,
            "currency" => "usd",
            "source" => $r->stripeToken,
            "description" => "Test payment from itsolutionstuff.com." 
        ]);
        $carts = cart::where('user_id',session::get('id'))->get();
        $order = new Order;
        $order->user_id = Session::get("id");
        $order->total = $total;
        $order->save();
        foreach ($carts as $product) {
        	$prod = Products::where('id', $product->product_id)->first();
        	$a = $prod->count;
        	Products::where('id',$product->product_id)
        	->update([
        		'count' => $a - $product->count
        	]);
        	$order_details = new Orderdetales;
        	$order_details->order_id = $order->id;
        	$order_details->product_id = $product->product_id;
        	$order_details->count = $product->count;
        	$order_details->save();
        }
        cart::where('user_id',session::get('id'))->delete();
        session::flash('normaluxarkvav','awesome');
        Session::flash ( 'success-message', 'Payment done successfully !' );
        return Redirect::to('conform');
        // return back();
    }
}
