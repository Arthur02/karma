<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		.verevimas{
			width: 80%;
			height: 250px;
			background: rgb(238,174,202);
			background: radial-gradient(circle, rgba(238,174,202,1) 0%, rgba(148,187,233,1) 100%);
			border-radius: 5px
		}
		.batna{
			background: #FFB200;
			color: white;
			text-decoration: none;
			border-radius: 10px;
			padding:10px;

		}
	</style>
</head>
<body>
	@if(isset($name))
	<div align="center">
	<img src="img/logo.png">
		<div class="verevimas" align="center">
		</div>
			<p style="font-family: sans-serif; font-size: 36px; color:#292929">Email Conformation</p>
			<p style="width: 40%; font-size:18px;font-family: sans-serif; color: #292929">		
				Hey {{ $name }} you're almost ready to start enjoying karma.
				Simply, click the big yellow button below to verify your
				email address.
			</p>
		<a class="batna" href="{{ url('/user/activate/'.$id.'/'.$verification_key) }}">Verify email address</a>
	</div>
	@else
	<div align="center">
		<img src="img/logo.png">
			<div class="verevimas" align="center">
			</div>
				<p style="font-family: sans-serif; font-size: 36px; color:#292929">Password Code</p>
				<p style="width: 40%; font-size:18px;font-family: sans-serif; color: #292929">		
					Your Code is <span style="color:#ebb434"> {{ $randomString }} </span> Copy this and paste to right place<br>
					if it isn't you ignore this message.
				</p>
	</div>
	@endif
</body>
</html>